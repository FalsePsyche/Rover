﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RJCP.IO.Ports;
using RoverComms;
using Excel = Microsoft.Office.Interop.Excel;
using Parity = System.IO.Ports.Parity;
using StopBits = System.IO.Ports.StopBits;

// TODO: figure out how to get custom parms from command line into a function like the MoveRover() func

namespace ConsoleSandbox
{
    class Program
    {
        private static string PortName = "COM16";
        private static int Baud = 9600;
        private static Rover rover = new Rover();


        private static Dictionary<string, Command> Commands = new Dictionary<string, Command>()
        {
            // all keys must be 4 items long
            { "quit", new Command(new Action(Quit), "quit the rover control terminal")},
            { "help", new Command(new Action(Help), "show this help list")},
            { "stat", new Command(new Action(Status), "show the status of the rover")},
            { "conn", new Command(new Action(Connect), "connect to the rover")},
            { "move", new Command(new ActionMove<string>(MoveRover), "move the rover, type move help for move help")},
            { "sens", new Command(new Action<int>(GetSensorData), "int datacount, get live sensor data; repeat for datacount")},
            //{ "move h", new Command(new Action(MoveRoverHelp), "move commands help")},
        };

        private static Dictionary<string, Command> MoveCommands = new Dictionary<string, Command>()
        {
            { "re", new Command(new Action<int>(rover.MotionControl.Move.Backward), "int speed, move rover reverse")},
            { "fo", new Command(new Action<int>(rover.MotionControl.Move.Forward), "int speed, move rover forward")},
            { "st", new Command(new ActionStop(rover.MotionControl.Move.Stop), "void, stop rover")},
            { "rr", new Command(new Action<int>(rover.MotionControl.Move.RotateRight), "int speed, rotate rover right")},
            { "rl", new Command(new Action<int>(rover.MotionControl.Move.RotateLeft), "int speed, rotate rover left")},
            { "sr", new Command(new Action<int>(rover.MotionControl.Move.StrafeRight), "int speed, strafe rover right")},
            { "sl", new Command(new Action<int>(rover.MotionControl.Move.StrafeLeft), "int speed, strafe rover left")},
            { "he", new Command(new ActionHelp(MoveRoverHelp), "show this help menu")},
        };

        #region Commands /////////////////////////////////////////

        class Command
        {
            public Command(Delegate action, string desc)
            {
                this.Action = action;
                this.Description = desc;
            }
            public Delegate Action
            {
                get { return _Action; }
                set
                {
                    if (Equals(value, _Action)) return;
                    _Action = value;
                }
            }

            private Delegate _Action;

            public string Description
            {
                get { return _Description; }
                set
                {
                    if (Equals(value, _Description)) return;
                    _Description = value;
                }
            }

            private string _Description;
        }

        delegate bool ActionStop ();
        delegate void ActionHelp();
        delegate bool Action <in T> (int speed);
        delegate void ActionMove <in T> (string cmd);

        static void Quit()
        {
            listenForConsole = false;

            Console.WriteLine("bye");
            Thread.Sleep(1000); // for dramatic effect
        }

        static void Help()
        {
            Console.WriteLine($"\n\n command\n      action or description");

            foreach (var cmd in Commands)
            {
                Console.WriteLine($" {cmd.Key}\n        {cmd.Value.Description}");
            }
        }

        static void Status()
        {
            Console.WriteLine("todo write status here");
        }

        static void Connect()
        {
            var success = rover.Comms.ConnectToRover();
            if (success)
            {
                Console.WriteLine($"connected to rover on port {rover.Comms.PortName}");
                //var sensorData = comms.RequestSensorDataItemFromRover();
                //var sensorDataItem = new SensorDataItem(sensorData);
            }
            else
            {
                Console.WriteLine($"failed to connected to rover");
            }
        }

        static void MoveRoverHelp()
        {
            Console.WriteLine($"\n\n move command\n      value, action or description");

            foreach (var cmd in MoveCommands)
            {
                Console.WriteLine($" {cmd.Key}\n        {cmd.Value.Description}");
            }
        }

        static void MoveRover(string cmd_input)
        {
            if (cmd_input.Length < 2)
            {
                ShowCommandError(cmd_input);
            }
            Command cmd;
            var found = MoveCommands.TryGetValue(cmd_input.Trim().Substring(0, 2), out cmd); // find the command's function, if it has one
            if (found)
            {
                var parms = cmd.Action.Method.GetParameters();
                if (parms.Length == 0)
                {
                    cmd.Action.DynamicInvoke();
                }
                else if (parms.Length == 1) // if we are calling a method with one argument
                {
                    int parm;
                    if (int.TryParse(cmd_input.Trim().Substring(2), out parm))
                    {
                        cmd.Action.DynamicInvoke(parm);
                    }
                    else
                    {
                        ShowCommandError(cmd_input);
                    }
                }
            }
            else
            {
                ShowCommandError(cmd_input);
            }
        }

        static bool GetSensorData(int repeat)
        {
            var success = false;
            var watch = new Stopwatch();
            watch.Start();
            var data = rover.Comms.RequestSensorDataItemFromRover();
            var sensordata = new SensorDataItem(data);

            Console.WriteLine($"Timestamp ms | Gyro X Y Z | Magnetometer X Y Z | Accelerometer X Y Z" +
                              $"\n{watch.ElapsedMilliseconds:00,000} | {getSensorPrint(sensordata)}");
            for (int i = 1; i < repeat; i++)
            {
                Thread.Sleep(200);
                data = rover.Comms.RequestSensorDataItemFromRover();
                sensordata = new SensorDataItem(data);
                Console.WriteLine($"{watch.ElapsedMilliseconds:00,000} | {getSensorPrint(sensordata)}");
            }

            watch.Stop();

            return success;
        }

        private static string getSensorPrint(SensorDataItem sensordata)
        {
            var print = $"{getFormatted(sensordata.Gyro.X)} {getFormatted(sensordata.Gyro.Y)} {getFormatted(sensordata.Gyro.Z)}" +
                        $" | {getFormatted(sensordata.Magnetic.X)} {getFormatted(sensordata.Magnetic.Y)} {getFormatted(sensordata.Magnetic.Z)}" +
                        $" | {getFormatted(sensordata.Acceleration.X)} {getFormatted(sensordata.Acceleration.Y)} {getFormatted(sensordata.Acceleration.Z)}";
            return print;
        }
        private static void ShowCommandError(string cmd_input)
        {
            Console.WriteLine($"error: the move command {cmd_input} is not valid, type help for help");
        }

        private static string getFormatted(object value)
        {
            var thing = Convert.ToSingle(value);
            return $"{thing: 00.00;-00.00}";
        }

        #endregion Commands //////////////////////////////////////

        private static bool listenForConsole = true;
        //static SerialCommunications comms = new SerialCommunications();
        static void Main(string[] args)
        {
            //rover = new Rover();
            //Console.SetWindowSize(320, 600);
            //Console.WindowWidth = 320;
            Console.WriteLine("Rover Control");
            Console.WriteLine("quit to quit");
            Console.WriteLine("return to send command");
            Console.WriteLine("help for help");
            while (listenForConsole)
            {
                var full_cmd = Console.ReadLine();
                var cmd_key = "";
                //RuntimeHelpers.TryCode code = new RuntimeHelpers.TryCode((cmd_key => full_cmd.Substring(0, 4)));
                try
                {
                    cmd_key = full_cmd.Substring(0, 4);
                }
                catch (Exception)
                {

                }
                Command cmd;
                var found = Commands.TryGetValue(cmd_key, out cmd); // find the command's function, if it has one
                if (found)
                {
                    var parms = cmd.Action.Method.GetParameters();
                    //Console.WriteLine($"parms: {parms}");
                    var thing = cmd.Action.Method.GetGenericArguments();
                    if (parms.Length == 0)
                    {
                        cmd.Action.DynamicInvoke();
                    }
                    else if (parms.Length == 1)
                    {
                        if (parms[0].ParameterType == typeof(string))
                        {
                            cmd.Action.DynamicInvoke(full_cmd.Substring(4));
                        }
                        else if (parms[0].ParameterType == typeof(int))
                        {
                            int parm;
                            if (int.TryParse(full_cmd.Substring(4).Trim(), out parm))
                            {
                                cmd.Action.DynamicInvoke(parm);
                            }
                            else
                            {
                                ShowCommandError(full_cmd);
                            }
                            //cmd.Action.DynamicInvoke(full_cmd.Substring(4));
                        }
                    }
                }
                else
                {
                    Console.WriteLine($"error: the command {full_cmd} is not valid, type h for help");
                }
            }

            //Sensor sensor = new Sensor();
            // Try saved port name first
            //var hardware = CheckPort(PortName);
            //if(CheckPort(PortName))
            // search all available COM ports for the instrument
            //string[] ports = SerialPortStream.GetPortNames();
            //for (int i = 0; i < ports.Length; i++)
            //{
            //    Console.WriteLine($"Checking serial port: {ports[i]}...");
            //    if (comms.CheckPort(ports[i], Baud))
            //    {
            //        PortName = ports[i];

            //        break;
            //    }
            //}


            //var port = comms.AttemptConnection(PortName, Baud);

            //if (port != null)
            //{
            //    if (!port.IsDisposed && port.IsOpen)
            //    {
            //        var watch = new Stopwatch();
            //        // todo send a message
            //        byte[] msg = new byte[] {58, 2, 0, 117};

            //        port.Write(msg, 0, msg.Length);
            //        watch.Start();
            //        while (port.BytesToRead == 0)
            //        {
            //            Thread.Sleep(1);
            //        }
            //        watch.Stop();
            //        Console.WriteLine($"Rover response time (ms): {watch.ElapsedMilliseconds}");

            //        Console.WriteLine($"Bytes to read: {port.BytesToRead}");



            //        var bytes = new List<byte>();
            //        var len = 100;
            //        while (bytes.Count < len + 4) // loop until the EOL is found
            //        {
            //            for (int i = 0; i < port.BytesToRead; i++)
            //            {
            //                bytes.Add((byte) port.ReadByte());
            //                Console.WriteLine($"{bytes[i]}");
            //            }

            //            len = comms.GetMsgLength(bytes.ToArray());
            //            var EOL = bytes.GetRange(bytes.Count - 5, 4);

            //            //Console.WriteLine($"Checking EOL at: [{string.Join(",", EOL)}]");
            //            //var temp = System.BitConverter.ToSingle(EOL.ToArray(), 0);
            //            //Console.WriteLine($"EOL: {temp}");
            //            //var msg1 = bytes.GetRange(3,len+3);
            //            if (bytes[bytes.Count - 1] == 200)
            //            {
            //                break;
            //            }
            //        }
            //        Console.WriteLine($"Byte count: {bytes.Count}");

            //        //Console.WriteLine($"Bytes: [{string.Join(",", bytes)}]");
            //        Console.WriteLine($"Bytes:");
            //        var count = 0;
            //        for (int i = 0; i < bytes.Count - 1; i += 4)
            //        {
            //            var thisLIne = "";
            //            foreach (var b in bytes.GetRange(i, 4))
            //            {
            //                thisLIne += $" {b:D3}";
            //            }
            //            Console.WriteLine($"{count:d2} {thisLIne}");
            //            count++;
            //        }

            //        Console.WriteLine($"EOL: {BitConverter.ToSingle(bytes.ToArray(), bytes.Count - 4)}");

            //        Console.WriteLine($"Status: {bytes[1]}");
            //        Console.WriteLine($"Message Byte Length: {bytes[3]}");

            //        Console.WriteLine($"Message Values:");
            //        for (int i = 4; i < bytes.Count - 1; i += 4) // start at 4 to ignore the Header
            //        {
            //            var val = BitConverter.ToSingle(bytes.GetRange(i, 4).ToArray(), 0);
            //            Console.WriteLine($"{val:0000.0000}");
            //        }

            //        sensor = new SensorDataItem(bytes.GetRange(4, 64));
            //    }
            //    else
            //    {
            //        Console.WriteLine($"{PortName} is CLOSED!");
            //        port.Dispose();
            //    }
            //    port?.Dispose();
            //}
            //port?.Dispose();
            //Console.ReadLine();

            //var names = SerialPortStream.GetPortNames();
            //var port1 = new SerialPortStream(PortName, Baud);
        }
    }
}