﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSandbox
{
    public class ReceiveMessage
    {
        public ReceiveMessage()
        {

        }

        public ReceiveMessage(int status, int length)
        {
            this.Status = status;
            this.Length = length;
        }

        public int Status
        {
            get { return _Status; }
            set
            {
                if (Equals(value, _Status)) return;
                _Status = value;
                //OnPropertyChanged(nameof(_Status));
            }
        }

        private int _Status;

        public int Length
        {
            get { return _Length; }
            set
            {
                if (Equals(value, _Length)) return;
                _Length = value;
                //OnPropertyChanged(nameof(_Length));
            }
        }

        private int _Length;

    }
}
