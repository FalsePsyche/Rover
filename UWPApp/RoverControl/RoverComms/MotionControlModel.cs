﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//NOTE:
// 1 = Counter ClockWise
// 2 = ClockWise

namespace RoverComms
{
    public class MotionControlModel
    {
        private SerialCommunications Comms;
        public MotionControlModel(SerialCommunications comms)
        {
            this.Comms = comms;
            this.Move = new MoveCommands(this.Comms);
        }

        public bool IsMoving
        {
            get { return _IsMoving; }
            set
            {
                if (Equals(value, _IsMoving)) return;
                _IsMoving = value;
                //OnPropertyChanged(nameof(_IsMoving));
            }
        }

        private bool _IsMoving;

        public MoveCommands Move
        {
            get { return _Move; }
            set
            {
                if (Equals(value, _Move)) return;
                _Move = value;
                //OnPropertyChanged(nameof(_Move));
            }
        }

        private MoveCommands _Move;

        #region Move Command Functions /////////////////////////////////////////

        public class MoveCommands
        {
            private SerialCommunications Comms;
            public MoveCommands(SerialCommunications comms)
            {
                this.Comms = comms;
            }

            public bool Forward(int speed)
            {
                //var success = false;
                // do all the things for moving the rover forward
                int[] dir = { 0, 2, 1, 0 };
                int[] speed_array = { speed, speed, speed, speed };
                return MoveCommand(dir, speed_array);
                //var motorPacket = new MotorPacket(dir, speed_array);

                //success = this.Comms.SendRaw(motorPacket.Packet);

                //return success;
            }

            public bool Backward(int speed)
            {
                //var success = false;
                // do all the things for moving the rover forward
                int[] dir = { 0, 1, 2, 0 };
                int[] speed_array = { speed, speed, speed, speed };
                return MoveCommand(dir, speed_array);
                //var motorPacket = new MotorPacket(dir, speed_array);
                //success = this.Comms.SendRaw(motorPacket.Packet);

                //return success;
            }

            public bool Stop()
            {
                //var success = false;
                // do all the things for moving the rover forward
                return MoveCommand(0, 0);
                //int[] dir = { 0, 0, 0, 0 };
                //int[] speed_array = { 0, 0, 0, 0 };
                //var motorPacket = new MotorPacket(dir, speed_array);
                //success = this.Comms.SendRaw(motorPacket.Packet);

                //return success;
            }

            /// <summary>
            /// Turns all wheels clockwise to rotate the rover so that it turns counter clockwise(birds eye view).
            /// </summary>
            /// <param name="speed">The speed of the movement.</param>
            /// <returns></returns>
            public bool RotateRight(int speed)
            {
                var success = false;

                // do all the things for moving the rover forward
                success = MoveCommand(1, speed);

                return success;
            }

            /// <summary>
            /// Turns all wheels counter clockwise to rotate the rover so that it turns clockwise(birds eye view).
            /// </summary>
            /// <param name="speed">The speed.</param>
            /// <returns></returns>
            public bool RotateLeft(int speed)
            {
                var success = false;

                // do all the things for moving the rover forward
                success = MoveCommand(2, speed);

                return success;
            }

            /// <summary>
            /// Stops left and right motors and turns front and rear to strafe the rover to the right.
            /// </summary>
            /// <param name="speed">The speed.</param>
            /// <returns></returns>
            public bool StrafeRight(int speed)
            {
                return MoveCommand(new int[] { 1, 3, 3, 2}, new int[] {speed, speed, speed, speed});
            }

            public bool StrafeLeft(int speed)
            {
                return MoveCommand(new int[] {2, 3, 3, 1}, new int[] {speed, speed, speed, speed});
            }

            private bool MoveCommand(int dir, int speed)
            {
                int[] dir_array = {dir, dir, dir, dir};
                int[] speed_array = {speed, speed, speed, speed};
                return MoveCommand(dir_array, speed_array);
            }

            private bool MoveCommand(int[] direction, int[] speed)
            {
                var motorPacket = new MotorPacket(direction, speed);
                var success = this.Comms.SendRaw(motorPacket.Packet);
                Console.WriteLine($"packet sent: {success.PacketSent}");
                return success.Success;
            }
        }


        #endregion Move Command Functions //////////////////////////////////////
    }
}
