﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoverComms
{
    public class Sensor
    {
        //public SensorDataItem RequestSensorDataItemFromRover()
        //{
        //    byte[] msg = new byte[] { 58, 2, 0, 117 };

        //    return null;
        //}
    }

    /// <summary>
    /// represents the abstract data type of a packet of sensor data from rover
    /// </summary>
    public class SensorDataItem
    {
        // todo create ADT for packet data here.
        public SensorDataItem(List<byte> packet)
        {
            // packet must be 64 in length
            if (packet.Count < 64) throw new ArgumentException("packet size is too small for a SensorDataItem object");
            Header = BitConverter.ToSingle(packet.ToArray(), 0);
            this.Magnetic = new ThreeDofItem(packet.GetRange(4, 12));
            this.Gyro = new ThreeDofItem(packet.GetRange(16, 12));
            this.Acceleration = new ThreeDofItem(packet.GetRange(28, 12));
            this.Temperature = BitConverter.ToSingle(packet.ToArray(), 40);
            this.Pressure = BitConverter.ToSingle(packet.ToArray(), 44);
            this.Sonar = new SonarTrio(packet.GetRange(48, 12));
            this.EOL = BitConverter.ToSingle(packet.ToArray(), 60);
        }

        public float Header
        {
            get { return _header; }
            set
            {
                if (Equals(value, _header)) return;
                _header = value;
                //OnPropertyChanged(nameof(_header));
            }
        }

        private float _header;

        public ThreeDofItem Magnetic
        {
            get { return _Magnetic; }
            set
            {
                if (Equals(value, _Magnetic)) return;
                _Magnetic = value;
                //OnPropertyChanged(nameof(_Magnetic));
            }
        }

        private ThreeDofItem _Magnetic;

        public ThreeDofItem Gyro
        {
            get { return _Gyro; }
            set
            {
                if (Equals(value, _Gyro)) return;
                _Gyro = value;
                //OnPropertyChanged(nameof(_Gyro));
            }
        }

        private ThreeDofItem _Gyro;

        public ThreeDofItem Acceleration
        {
            get { return _Acceleration; }
            set
            {
                if (Equals(value, _Acceleration)) return;
                _Acceleration = value;
                //OnPropertyChanged(nameof(_Acceleration));
            }
        }

        private ThreeDofItem _Acceleration;

        public float Temperature
        {
            get { return _Temperature; }
            set
            {
                if (Equals(value, _Temperature)) return;
                _Temperature = value;
                //OnPropertyChanged(nameof(_Temperature));
            }
        }

        private float _Temperature;

        public float Pressure
        {
            get { return _Pressure; }
            set
            {
                if (Equals(value, _Pressure)) return;
                _Pressure = value;
                //OnPropertyChanged(nameof(_Pressure));
            }
        }

        private float _Pressure;

        public SonarTrio Sonar
        {
            get { return _sonar; }
            set
            {
                if (Equals(value, _sonar)) return;
                _sonar = value;
                //OnPropertyChanged(nameof(_sonar));
            }
        }

        private SonarTrio _sonar;

        public float EOL
        {
            get { return _EOL; }
            set
            {
                if (Equals(value, _EOL)) return;
                _EOL = value;
                //OnPropertyChanged(nameof(_EOL));
            }
        }

        private float _EOL;

        public class ThreeDofItem
        {
            public ThreeDofItem(List<byte> packet)
            {
                // packet length must be 12
                if (packet.Count < 12) throw new ArgumentException("packet size is too small for a ThreeDofItem object");
                this.X = BitConverter.ToSingle(packet.ToArray(), 0);
                this.Y = BitConverter.ToSingle(packet.ToArray(), 4);
                this.Z = BitConverter.ToSingle(packet.ToArray(), 8);
            }

            public ThreeDofItem(float x, float y, float z)
            {
                this.X = x;
                this.Y = y;
                this.Z = z;
            }

            public float X
            {
                get { return _X; }
                set
                {
                    if (Equals(value, _X)) return;
                    _X = value;
                    //OnPropertyChanged(nameof(_X));
                }
            }

            private float _X;

            public float Y
            {
                get { return _Y; }
                set
                {
                    if (Equals(value, _Y)) return;
                    _Y = value;
                    //OnPropertyChanged(nameof(_Y));
                }
            }

            private float _Y;

            public float Z
            {
                get { return _Z; }
                set
                {
                    if (Equals(value, _Z)) return;
                    _Z = value;
                    //OnPropertyChanged(nameof(_Z));
                }
            }

            private float _Z;
        }

        public class SonarTrio
        {
            public SonarTrio(List<byte> packet)
            {
                // packet length must be 12
                if (packet.Count < 12) throw new ArgumentException("packet size is too small for a SonarTrio object");
                this.Center = BitConverter.ToSingle(packet.ToArray(), 0);
                this.Right = BitConverter.ToSingle(packet.ToArray(), 4);
                this.Left = BitConverter.ToSingle(packet.ToArray(), 8);
            }

            public SonarTrio(float left, float center, float right)
            {
                this.Left = left;
                this.Center = center;
                this.Right = right;
            }

            public float Left
            {
                get { return _Left; }
                set
                {
                    if (Equals(value, _Left)) return;
                    _Left = value;
                    //OnPropertyChanged(nameof(_Left));
                }
            }

            private float _Left;

            public float Center
            {
                get { return _Center; }
                set
                {
                    if (Equals(value, _Center)) return;
                    _Center = value;
                    //OnPropertyChanged(nameof(_Center));
                }
            }

            private float _Center;

            public float Right
            {
                get { return _Right; }
                set
                {
                    if (Equals(value, _Right)) return;
                    _Right = value;
                    //OnPropertyChanged(nameof(_Right));
                }
            }

            private float _Right;
        }
    }
}