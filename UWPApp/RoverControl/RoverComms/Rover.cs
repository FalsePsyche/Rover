﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoverComms
{
    /// <summary>
    /// The abstract data type of a Rover object.
    /// </summary>
    public class Rover
    {
        public Rover()
        {
            this.Comms = new SerialCommunications();
            this.MotionControl = new MotionControlModel(this.Comms);
        }

        public SerialCommunications Comms
        {
            get { return _Comms; }
            set
            {
                if (Equals(value, _Comms)) return;
                _Comms = value;
                //OnPropertyChanged(nameof(_Comms));
            }
        }

        private SerialCommunications _Comms;

        public MotionControlModel MotionControl
        {
            get { return _MotionControl; }
            set
            {
                if (Equals(value, _MotionControl)) return;
                _MotionControl = value;
                //OnPropertyChanged(nameof(_MotionControl));
            }
        }

        private MotionControlModel _MotionControl;
    }
}
