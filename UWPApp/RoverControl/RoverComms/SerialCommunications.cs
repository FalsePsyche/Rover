﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RJCP.IO.Ports;

namespace RoverComms
{
    public class SerialCommunications
    {
        protected Mutex mutex = new Mutex(); // mutex to ensure only 1 command is processed at a time
        private SerialPortStream port = new SerialPortStream();
        public const int BaudRate = 9600;
        public const int ListenTimeout = 500; // milliseconds
        public string PortName;

        public List<byte> RequestSensorDataItemFromRover()
        {
            byte[] msg = new byte[] { 58, 2, 0, 117 };
            SendRaw(msg);
            var rx = ListenToRover();
            return rx.GetRange(4, 64);
        }

        /// <summary>
        /// Sends a byte stream to the VAS and returns the response from the instrument
        /// This function will automatically calculate and add the checksum to the end of the message
        /// and will also check the result checksum and check if the response matches the message
        /// </summary>
        /// <param name="cmd">Command sent - used to check return value</param>
        /// <param name="message">Byte stream to send to instrument</param>
        /// <returns>Data returned from the instrument</returns>
        //private List<byte> SendToVAS(VASCommand cmd, List<byte> message)
        //{
        //    bool haveMutex = false;
        //    try
        //    {
        //        if (mutex.WaitOne(Properties.Settings.Default.MaxLockWait))
        //        {
        //            haveMutex = true;

        //            // Clear any bytes that are waiting on the buffer before sending.
        //            try
        //            {
        //                int waiting = port.BytesToRead;
        //                if (waiting > 0)
        //                {
        //                    byte[] junkBuffer = new byte[waiting];
        //                    int waited = port.Read(junkBuffer, 0, waiting);

        //                    Debug.WriteLine("Cleared {0} junk bytes out of {1} waiting on input buffer.", waited,
        //                        waiting);
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                throw new VASCommunicationException("Unable to communicate with instrument.", ex);
        //            }
        //            Debug.WriteLine("Writing command {0} to MiniVAS", cmd);

        //            // Calculate a checksum for using the whole message and add it to the end
        //            ushort crc = CheckSum.GetCheckSum(message);
        //            message.AddRange(EndianBitConvertor.GetBytes(crc));
        //            //System.Diagnostics.Debug.WriteLine("Msg: " + ByteString(message));
        //            try
        //            {
        //                port.Write(message.ToArray(), 0, message.Count);
        //            }
        //            catch (Exception ex)
        //            {
        //                throw new VASCommunicationException("Unable to write to instrument.", ex);
        //            }
        //            // A delay is required or else the instrument throws back zeros
        //            // Not sure why that isn't ignored by port.Read?
        //            // Thread.Sleep(Properties.Settings.Default.CommDelay);
        //            // read five byte header
        //            byte[] headerBuffer = new byte[VASReturnMessageHeader.HeaderSize];
        //            Stopwatch sw = Stopwatch.StartNew();
        //            try
        //            {
        //                while (port.BytesToRead < headerBuffer.Length)
        //                {
        //                    if (sw.ElapsedMilliseconds > Properties.Settings.Default.ReadTimeout)
        //                    {
        //                        Console.WriteLine("{0} bytes are available out of expected {1} for header.", port.BytesToRead, headerBuffer.Length);
        //                        throw new TimeoutException("Timed out waiting for port");
        //                    }
        //                }
        //                port.Read(headerBuffer, 0, VASReturnMessageHeader.HeaderSize);
        //                sw.Stop();
        //            }
        //            catch (TimeoutException ex)
        //            {
        //                Console.WriteLine("Failed to receive entire header for {0}.  Received {1} bytes available.", cmd, port.BytesToRead);
        //                //Debugger.Break();
        //                throw new VASCommunicationException("Timed out reading header", ex);
        //            }
        //            VASReturnMessageHeader header = new VASReturnMessageHeader(headerBuffer);
        //            // Check status - ignore problems if command is immediate
        //            if (cmd != VASCommand.Immediate)
        //            {
        //                if (header.Status != VASStatus.Acknowledge)
        //                {
        //                    throw new VASCommunicationException("Instrument status error " +
        //                                                        header.Status.ToString());
        //                }
        //                // Check returned command is the same as the one we sent
        //                if (header.Command != cmd)
        //                {
        //                    throw new VASCommunicationException("Instrument returned " + header.Command.ToString() +
        //                                                        " from command " + cmd.ToString());
        //                }
        //            }
        //            byte[] dataBuffer = new byte[header.Size + 2]; // +2 for CRC
        //            sw = Stopwatch.StartNew();
        //            try
        //            {
        //                while (port.BytesToRead < dataBuffer.Length)
        //                {
        //                    if (sw.ElapsedMilliseconds > Properties.Settings.Default.ReadTimeout)
        //                    {
        //                        throw new TimeoutException("Timed out waiting for port");
        //                    }
        //                }
        //                port.Read(dataBuffer, 0, dataBuffer.Length);
        //            }
        //            catch (TimeoutException ex)
        //            {
        //                throw new VASCommunicationException("Timed out reading data", ex);
        //            }
        //            sw.Stop();
        //            // last two bytes of the data are the crc.
        //            ushort replyCrc = EndianBitConvertor.ToUInt16(dataBuffer, dataBuffer.Length - 2);

        //            // To compare CRCs we need to reconstruct the message minus the crc
        //            List<Byte> reply = new List<byte>(headerBuffer.Length + dataBuffer.Length);
        //            reply.AddRange(headerBuffer);
        //            reply.AddRange(dataBuffer);
        //            reply.RemoveRange(reply.Count - 2, 2);
        //            crc = CheckSum.GetCheckSum(reply);

        //            //System.Diagnostics.Debug.WriteLine("Reply: " + ByteString(reply));
        //            //System.Diagnostics.Debug.WriteLine("CRC: " + replyCrc + "\tExpected: " + crc);

        //            if (crc != replyCrc)
        //            {
        //                Debug.WriteLine("CRC failed.  Got {0}, expected {1}", replyCrc, crc);
        //                throw new VASCommunicationException(string.Format("CRC failed.  Got {0}, expected {1}", replyCrc, crc));
        //            }

        //            // Remove the header
        //            reply.RemoveRange(0, headerBuffer.Length);
        //            return reply;
        //        }
        //        else
        //        {
        //            throw new VASCommunicationException("Communication is deadlocked");
        //            ;
        //        }
        //    }
        //    catch (VASCommunicationException e)
        //    {
        //        //Debugger.Break();
        //        System.Diagnostics.Debug.WriteLine(e.Message);

        //        if (exceptionHandler != null)
        //        {
        //            exceptionHandler(e);
        //            error = true;
        //            return new List<byte>();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }
        //    finally
        //    {
        //        if (haveMutex)
        //        {
        //            mutex.ReleaseMutex();
        //        }
        //    }
        //}

        public bool ConnectToRover()
        {
            if (this.port.IsOpen) return true; //already open so return
            var name = FindRoverCommPort();
            try
            {
                port = new SerialPortStream(name, BaudRate);
                port.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine($"failure to open serial stream\n     {e.Message}");
            }
            return port.IsOpen;
        }

        private string FindRoverCommPort()
        {
            var portName = "";
            // search all available COM ports for the instrument
            string[] ports = SerialPortStream.GetPortNames();
            for (int i = 0; i < ports.Length; i++)
            {
                Console.WriteLine($"Checking serial port: {ports[i]}...");
                if (this.CheckPort(ports[i], BaudRate))
                {
                    portName = ports[i];
                    ConfirmRoverPortWithHandShakeRequest(portName);
                    break;
                }
            }

            this.PortName = portName;
            return portName;
        }

        private byte[] _handShakeRequest = new byte[] {58, 0, 0, 117};

        private bool ConfirmRoverPortWithHandShakeRequest(string portName)
        {
            var success = false;

            //SendRaw(_handShakeRequest);
            var bytes = ListenToRover();
            Debug.WriteLine($"handshake: {bytes}");

            return success;
        }

        private string getStringFromPacket(byte[] packet)
        {
            var msg = "[";

            var bytes = packet.ToList();
            foreach (var b in bytes)
            {
                msg += $" {b:D3}";
            }
            msg += " ]";
            return msg;
        }

        public SendOutcomeItem SendRaw(byte[] packet)
        {
            //var outcome = new SendOutcomeItem();
            if (string.IsNullOrEmpty(this.port.PortName)) return new SendOutcomeItem("", false); // not yet connected so return null;
            if (!port.IsOpen) return new SendOutcomeItem("", false); // not yet connected so return null;
            port.Write(packet, 0, packet.Length);
            Debug.WriteLine($"packet sent: {getStringFromPacket(packet)}");
            return new SendOutcomeItem(getStringFromPacket(packet), true);
        }

        public class SendOutcomeItem
        {
            public SendOutcomeItem()
            {
            }
            public SendOutcomeItem(string packetSent, bool success)
            {
                this.PacketSent = packetSent;
                this.Success = success;
            }

            public string PacketSent
            {
                get { return _packetSent; }
                set
                {
                    if (Equals(value, _packetSent)) return;
                    _packetSent = value;
                    //OnPropertyChanged(nameof(_packetSent));
                }
            }

            private string _packetSent;

            public bool Success
            {
                get { return _Success; }
                set
                {
                    if (Equals(value, _Success)) return;
                    _Success = value;
                    //OnPropertyChanged(nameof(_Success));
                }
            }

            private bool _Success;
        }

        private List<byte> ListenToRover()
        {
            var bytes = new List<byte>();
            var len = 100;
            var watch = new Stopwatch();
            watch.Start();
            while (watch.ElapsedMilliseconds < ListenTimeout)
            {
                bytes.Add((byte)port.ReadByte());
                if(bytes.Count >= 4) len = this.GetMsgLength(bytes.ToArray());
                if (bytes.Count >= len + 4) break;
            }
            return bytes;
        }

        public object Send()
        {

            return null;
        }

        private int GetMsgLength(byte[] bytes)
        {
            if (bytes.Length < 4) return -1;
            var length = 0;
            length = (int)bytes[3];
            //length = System.BitConverter.ToInt16(bytes, 3);
            return length;
        }

        /// <summary>
        /// Attempts to connect to specified serial port, returns null if connection fails.
        /// </summary>
        /// <param name="portName">Name of the port.</param>
        /// <param name="baudrate">The baudrate.</param>
        /// <returns></returns>
        public SerialPortStream AttemptConnection(string portName, int baudrate)
        {
            SerialPortStream port = null;
            try
            {
                port = new SerialPortStream(portName, baudrate);

                port.Open();
                Console.WriteLine($"{portName} connection SUCCESS!");
            }
            catch (Exception e)
            {
                Console.WriteLine($"{portName} connection FAIL!");
                port?.Dispose();
            }
            return port;
        }

        /// <summary>
        /// Look for an instrument on the given port and return the hardware type found
        /// </summary>
        /// <param name="portName">name of port to check</param>
        /// <param name="baudrate"></param>
        /// <returns>Type of hardware found at the specified port</returns>
        public bool CheckPort(string portName, int baudrate)
        {
            Debug.WriteLine(portName);
            // Set up serial communications with instrument
            var portCheck = new SerialPortStream(portName, baudrate)
            {
                ReadTimeout = 500,
                WriteTimeout = 500
            };
            try
            {
                portCheck.Open();
                portCheck.Close();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                // No hardware??
                portCheck?.Close();
                portCheck?.Dispose();
                return false;
            }
            portCheck.Dispose();

            return true;
        }
    }

    public class Packet
    {
        public bool SendPacket(byte[] packet)
        {
            var success = false;
            if (packet.Length > 2)
            {

            }
            return success;
        }
    }

    public class MotorPacket : Packet
    {
        public MotorPacket(int[] direction, int[] speed)
        {
            if(direction.Length != 4) throw new ArgumentException("direction array must be of size 4");
            else if(speed.Length != 4) throw new ArgumentException("speed array must be of size 4");
            this.Motor1 = new MotorCommand(getMotorAction(direction[0]), speed[0]);
            this.Motor2 = new MotorCommand(getMotorAction(direction[1]), speed[1]);
            this.Motor3 = new MotorCommand(getMotorAction(direction[2]), speed[2]);
            this.Motor4 = new MotorCommand(getMotorAction(direction[3]), speed[3]);
            this.Packet = CompilePacket(this.Motor1, this.Motor2, this.Motor3, this.Motor4);
        }
        public MotorPacket(byte[] packet)
        {
            if(packet.Length < 8) throw new ArgumentException("Not enough parameters for a Motor Packet");
            this.Motor1 = new MotorCommand(getMotorAction(packet[0]), packet[1]);
            this.Motor2 = new MotorCommand(getMotorAction(packet[2]), packet[3]);
            this.Motor3 = new MotorCommand(getMotorAction(packet[4]), packet[5]);
            this.Motor4 = new MotorCommand(getMotorAction(packet[6]), packet[7]);
            this.Packet = CompilePacket(this.Motor1, this.Motor2, this.Motor3, this.Motor4);
        }

        private MotorCommand Motor1;
        private MotorCommand Motor2;
        private MotorCommand Motor3;
        private MotorCommand Motor4;
        public byte[] Packet;

        private byte[] CompilePacket(MotorCommand m1, MotorCommand m2, MotorCommand m3, MotorCommand m4)
        {
            var count = 0;
            byte[] packet = new[]
            {
                (byte) 58,
                (byte) 1,
                (byte) m1.Direction,
                (byte) m1.Speed,
                (byte) m2.Direction,
                (byte) m2.Speed,
                (byte) m3.Direction,
                (byte) m3.Speed,
                (byte) m4.Direction,
                (byte) m4.Speed,
                (byte) 117
            };
            return packet;
        }

        public enum MotorAction
        {
            Stop,
            Forward,
            Reverse,
            Unchanged
        }

        private MotorAction getMotorAction(int action)
        {
            switch (action)
            {
                case 0:
                    return MotorAction.Stop;
                case 1:
                    return MotorAction.Forward;
                case 2:
                    return MotorAction.Reverse;
                case 3:
                    return MotorAction.Unchanged;
                default:
                    return MotorAction.Unchanged;
            }
        }

        public class MotorCommand
        {
            public MotorCommand(MotorAction dir, int speed)
            {
                this.Direction = dir;
                this.Speed = speed;
            }

            public MotorAction Direction;
            public int Speed;
        }

        //def __init__(self, m1_direction, m1_speed, m2_direction, m2_speed,
        //        m3_direction, m3_speed, m4_direction, m4_speed):
        //    """
        //Parameters
        //----------
        //m1_speed: int
        //0 - 100%
        //m2_speed: int
        //0 - 100%
        //m1_direction: int
        //0 = stop, 1 = forward, 2 = backward, 3 = unchanged(do not change motor direction or speed)
        //m2_direction: int
        //0 = stop, 1 = forward, 2 = backward, 3 = unchanged(do not change motor direction or speed)
        //m3_speed: int
        //0 - 100%
        //m4_speed: int
        //0 - 100%
        //m3_direction: int
        //0 = stop, 1 = forward, 2 = backward, 3 = unchanged(do not change motor direction or speed)
        //m4_direction: int
        //0 = stop, 1 = forward, 2 = backward, 3 = unchanged(do not change motor direction or speed)
        //"""
        //super().__init__(1)
        //self.command1 = m1_direction
        //    self.command2 = m1_speed
        //    self.command3 = m2_direction
        //    self.command4 = m2_speed
        //    self.command5 = m3_direction
        //    self.command6 = m3_speed
        //    self.command7 = m4_direction
        //    self.command8 = m4_speed
        //    self.CompilePacket()

        //def CompilePacket(self):
        //self.message.append(self.header)
        //    self.message.append(self.destination)
        //self.message.append(self.command1)
        //    self.message.append(self.command2)
        //self.message.append(self.command3)
        //    self.message.append(self.command4)
        //self.message.append(self.command5)
        //    self.message.append(self.command6)
        //self.message.append(self.command7)
        //    self.message.append(self.command8)
        //self.message.append(117)  # need this because arduino relies on it to stop reading the serial
    }
}
