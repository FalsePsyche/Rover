#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif
// Which pin on the Arduino is connected to the NeoPixels?
#define PIN            6
// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      40

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRBW + NEO_KHZ800);

//40 leds * 60mA = 2400mA draw
//40 leds * 20mA = 800mA draw

int bluePxlArray[NUMPIXELS] = { 0 };
int prevBlue[NUMPIXELS] = { 0 }; // using these to save pre set pixel values for dimming
int redPxlArray[NUMPIXELS] = { 0 };
int greenPxlArray[NUMPIXELS] = { 0 };

int brightness = 192; // 256 max - if on USB don't go above 192

void setup()
{
  Serial.begin(9600);
  randomSeed(analogRead(0));

  pixels.begin(); // This initializes the NeoPixel library.
  pixels.setBrightness(brightness);
}

int loopCount = 0;
int color = 0; //0 is off, 1 is blue and 2 is red
void loop()
{

  

  String thing = Serial.readString();

Serial.println(thing);
  
  if (thing == "2") {
    color = 2;
  } else if (thing == "1") {
    color = 1;
  } else if (thing == "0") {
    color = 0;
  }


  if (color == 1)
  {
    for (int i = 0; i < 40; i++)
    {
      pixels.setPixelColor(i, pixels.Color(0, 0, 255));//blue
    }
    pixels.show();
  }
  if (color == 2)
  {
    for (int i = 0; i < 40; i++)
    {
      pixels.setPixelColor(i, pixels.Color(255, 0, 0));//red
    }
    pixels.show();
  } else if(color == 0)
  {
    for (int i = 0; i < 40; i++)
    {
      pixels.setPixelColor(i, pixels.Color(0, 0, 0));
    }
    pixels.show();
  }


  //Lightning();
}
