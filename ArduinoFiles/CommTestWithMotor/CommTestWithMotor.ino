/*
  This is a test sketch for the Adafruit assembled Motor Shield for Arduino v2
  It won't work with v1.x motor shields! Only for the v2's with built in PWM
  control

  For use with the Adafruit Motor Shield v2
  ---->  http://www.adafruit.com/products/1438
*/

#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_PWMServoDriver.h"

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
// Or, create it with a different I2C address (say for stacking)
// Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x61);

// Select which 'port' M1, M2, M3 or M4. In this case, M1
Adafruit_DCMotor *Motor1 = AFMS.getMotor(1);
Adafruit_DCMotor *Motor2 = AFMS.getMotor(2);
// You can also make another motor on port M2
//Adafruit_DCMotor *myOtherMotor = AFMS.getMotor(2);

const int m1AnalogPin = 0;
const int m2AnalogPin = 1;

int m1sensorVal = 0;
int m2sensorVal = 0;
int m1speedVal = 0;
int m2speedVal = 0;

long prevMillis = 0;
long interval = 1000;

void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("Adafruit Motorshield v2 - DC Motor test!");

  AFMS.begin();  // create with the default frequency 1.6KHz
  //AFMS.begin(1000);  // OR with a different frequency, say 1KHz

  // Set the speed to start, from 0 (off) to 255 (max speed)
  Motor1->setSpeed(255);
  Motor1->run(FORWARD);
  Motor1->run(BACKWARD);
  // turn on motor
  Motor1->run(RELEASE);

  Motor2->setSpeed(255);
  Motor2->run(FORWARD);
  Motor2->run(BACKWARD);
  // turn on motor
  Motor2->run(RELEASE);

}


//trying to get this to work with 2 DC motors and 2 pots
void loop() {
  unsigned long currentMillis = millis();
  //  uint8_t i;
  m1sensorVal = analogRead(m1AnalogPin);
  m2sensorVal = analogRead(m2AnalogPin);

  //speedVal = getSpeedVal();

    if (currentMillis - prevMillis > interval)
    {
      prevMillis = currentMillis;
  
	  updateMotor1();
	  updateMotor2();
    }

  //  Motor1->setSpeed(0);
  //
  //  for (i = 0; i < 255; i++) {
  //    Motor1->setSpeed(i);
  //    delay(10);
  //  }
  //  for (i = 255; i != 0; i--) {
  //    Motor1->setSpeed(i);
  //    delay(10);
  //  }
  //  Serial.print("tock");
  //
  //  Motor1->run(BACKWARD);
  //  for (i = 0; i < 255; i++) {
  //    Motor1->setSpeed(i);
  //    delay(10);
  //  }
  //  delay(1000);
  //  for (i = 255; i != 0; i--) {
  //    Motor1->setSpeed(i);
  //    delay(10);
  //  }
  //  delay(500);
  //
  //  //Serial.print("tech");
  //  Motor1->run(RELEASE);
  //  delay(1000);


}

// the idea for this is to split 1 analog stream to control 2 directions and speed of 1 motor
void updateMotor2()
{
	Serial.println("\n//////////////	begin motor 2		//////////////");
	int _sensorVal = m2sensorVal;
	int _speedVal = 0;
	// i want 500 to be the slowest speed of FORWARD
	// and 523 to be the slowest speed of REVERSE
	// 0 should be the fastest speed of FORWARD
	//1023 should be the fastest speed of REVERSE
	// anything inbtwn 500 and 523 (non-inclusive) should be NO MOTION

	// FORWARD motion
	if (_sensorVal <= 500)
	{
		Motor2->run(FORWARD);
		//_speedVal = _sensorVal / 2;
		int invert = (_sensorVal - 500) * (-1); // invert number for correct speed
		double speedPercent = (double)invert / 500.0; // get the over all percentage of speed
		int thisSpeed = speedPercent * 255;
		m2speedVal = thisSpeed;
		Motor2->setSpeed(m2speedVal);

		Serial.println("\n//////////////	FORWARD		//////////////");
		Serial.print("invert: ");
		Serial.println(invert);
		Serial.print("SpeedPercent: ");
		Serial.println(speedPercent);
		Serial.print("thisSpeed: ");
		Serial.println(thisSpeed);
	}
	//NO MOTION
	else if (_sensorVal > 500 && _sensorVal < 523)
	{
		Motor2->run(RELEASE);
		Motor2->setSpeed(0);
		Serial.println("\n//////////////	RELEASE		//////////////");
	}
	//FORWARD MOTION
	else if (_sensorVal >= 523)
	{
		Motor2->run(BACKWARD);
		int invert = (_sensorVal - 523);// *(-1); // invert and offset number for correct speed
		double speedPercent = (double)invert / 500.0; // get the over all percentage of speed
		int thisSpeed = speedPercent * 255;
		m2speedVal = thisSpeed;
		Motor2->setSpeed(m2speedVal);

		Serial.println("\n//////////////	BACKWARD		//////////////");
		Serial.print("invert: ");
		Serial.println(invert);
		Serial.print("SpeedPercent: ");
		Serial.println(speedPercent);
		Serial.print("thisSpeed: ");
		Serial.println(thisSpeed);
	}


	Serial.print("Sensor: ");
	Serial.println(m2sensorVal);
	Serial.print("Speed: ");
	Serial.println(m2speedVal);
	Serial.println("\n//////////////	end motor 2		//////////////");
}

// the idea for this is to split 1 analog stream to control 2 directions and speed of 1 motor
void updateMotor1()
{
	Serial.println("\n//////////////	begin motor 1		//////////////");
  int _sensorVal = m1sensorVal;
  int _speedVal = 0;
  // i want 500 to be the slowest speed of FORWARD
  // and 523 to be the slowest speed of REVERSE
  // 0 should be the fastest speed of FORWARD
  //1023 should be the fastest speed of REVERSE
  // anything inbtwn 500 and 523 (non-inclusive) should be NO MOTION

  // FORWARD motion
  if (_sensorVal <= 500)
  {
    Motor1->run(FORWARD);
    //_speedVal = _sensorVal / 2;
    int invert = (_sensorVal - 500) * (-1); // invert number for correct speed
    double speedPercent = (double)invert / 500.0; // get the over all percentage of speed
    int thisSpeed = speedPercent * 255;
	m1speedVal = thisSpeed;
    Motor1->setSpeed(m1speedVal);

	Serial.println("\n//////////////	FORWARD		//////////////");
	Serial.print("invert: ");
	Serial.println(invert);
	Serial.print("SpeedPercent: ");
	Serial.println(speedPercent);
	Serial.print("thisSpeed: ");
	Serial.println(thisSpeed);
  }
  //NO MOTION
  else if (_sensorVal > 500 && _sensorVal < 523)
  {
    Motor1->run(RELEASE);
	Motor1->setSpeed(0);
	Serial.println("\n//////////////	RELEASE		//////////////");
  }
  //FORWARD MOTION
  else if (_sensorVal >= 523)
  {
    Motor1->run(BACKWARD);
	int invert = (_sensorVal - 523);// *(-1); // invert and offset number for correct speed
    double speedPercent = (double)invert / 500.0; // get the over all percentage of speed
    int thisSpeed = speedPercent * 255;
	m1speedVal = thisSpeed;
	Motor1->setSpeed(m1speedVal);

	Serial.println("\n//////////////	BACKWARD		//////////////");
	Serial.print("invert: ");
	Serial.println(invert);
	Serial.print("SpeedPercent: ");
	Serial.println(speedPercent);
	Serial.print("thisSpeed: ");
	Serial.println(thisSpeed);
  }


  Serial.print("Sensor: ");
  Serial.println(m1sensorVal);
  Serial.print("Speed: ");
  Serial.println(m1speedVal);
  Serial.println("\n//////////////	end motor 1		//////////////");
}

int getSpeedVal()
{
  int speedVal = m1sensorVal / 4;
  return speedVal;
}


void setDirection()
{

}


