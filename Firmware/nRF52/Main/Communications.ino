// This file is for communications between the raspberry pi and the arduino (this source code is firmware for the arduino)

///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma region Messages
typedef struct ErrorMessageStruct
{
    float header;       // 343.343
    float origin;       // origin of error
    float errorMessage; // detail of error message
};

typedef union ErrorPacket {
    ErrorMessageStruct error;
    byte I2CPacket[sizeof(ErrorMessageStruct)];
};
ErrorPacket errorPacket;
#pragma endregion Messages
///////////////////////////////////////////////////////////////////////////////////////////////////

//I'm pretty confident these are useless.. actually we can use them for error responses for now
typedef struct ResponseStruct
{
    //float responseType; // error, warning or message/information
    float first;
    /*float second;
	float third;*/
};
typedef union ResponsePacket {
    ResponseStruct ackstruct;
    byte ackpacket[sizeof(ResponseStruct)];
};

#define HEADER 70

//send acknowledgment response
void SendResponse(int responseType)
{
    //init packet response
    ResponsePacket response_packet;

    switch (responseType)
    {
    case 0:                                          // ack response
                                                     //response_packet.ackstruct.responseType = information; // message type
        response_packet.ackstruct.first = 1970.1970; // ack response
        break;
    case 1:                                      // motor command response
                                                 //response_packet.ackstruct.responseType = information; // message type
        response_packet.ackstruct.first = 1.2;   // 1 and 2 for motors 1 and 2
    case 2:                                      // sensor data response
                                                 //response_packet.ackstruct.responseType = information; // message type
        response_packet.ackstruct.first = 10.10; // 10 for 10DOF board
        break;
    case 3:                                              // UNKNOWN command (usually means header of packet does not equal 58)
                                                         // respond with error message 343
                                                         //response_packet.ackstruct.responseType = error; // 0
        response_packet.ackstruct.first = unknownHeader; // 0
        break;
    default:
        //response_packet.ackstruct.responseType = error;
        break;
    }

    int acksize = sizeof(response_packet.ackstruct);
    byte header[4] = {0, 255, 0, sizeof(acksize)};
    Serial.write(header, sizeof(header));
    Serial.write(response_packet.ackpacket, acksize);
}

#define PacketLength 11

byte RxPacket[PacketLength];
byte MotorCommand[PacketLength];
void GetSensorData(int sensortype);
byte AllDataPacket[64];

extern void Comms_Init();

void Comms_Init()
{
    setNewColors(0, 0, 0, 255, 0, 1);
}

extern void Comms_SysTick();

void Comms_SysTick(void)
{
    if (Serial.available() > 0)
    {
        Serial.readBytesUntil(117, rawPacket, PacketLength);
        memcpy(RxPacket, rawPacket, sizeof(rawPacket));
        // Serial.write(RxPacket, sizeof(RxPacket)); // send back what we get for debugging
        // Serial.write(45); // send back what we get for debugging
        byte packet[PacketLength];
        memcpy(packet, RxPacket, sizeof(packet));

        if (packet[0] == 58)
        {
            setNewColors(0, 0, 255, 0, 0, 1); // indicate good packet

            //use Packet information to do something
            if (packet[1] == 1) // going to motors
            {
                //SendResponse(packet[1]); // data is valid
                // Motion motion = new Motion();
                // motion.MoveRover(packet);
                memcpy(MotorCommand, RxPacket, sizeof(MotorCommand));
                // MotorCommand = packet;
            }
            // PARAMATER THINGS
            else if (packet[1] == 0) // going to arduino parms
            {
                setNewColors(2, 0, 0, 0, 255, 1);
                if (packet[2] == 0) // Handshake request
                {
                    //request to talk
                    SendResponse(0);
                }
                else if (packet[2] == 1) // LED Brightness Control
                {
                    setNewBrightness((int)packet[3]);
                }
                else if (packet[2] == 2) // LED Color Control
                {
                    // update LED color values
                    int targetLED = (int)packet[3];
                    int newRed = (int)packet[4];
                    int newGreen = (int)packet[5];
                    int newBlue = (int)packet[6];
                    int newWhite = (int)packet[7];
                    setNewColors(targetLED, newRed, newGreen, newBlue, newWhite, 1);
                }
                else if (packet[2] == 3) // Wait interval parameter
                {
                    //LoopWait parm change request
                    LoopWait = (long)packet[3]; // shouldn't go lower than 50ms
                }
                else if (packet[2] == 4) // SonarWait parameter
                {
                    //update delay time for the sonar sensors
                    // SonarWait = (int)packet[3]; // shouldn't go lower than 50ms
                }
                else if (packet[2] == 5) // min motor speed
                {
                    // MotorMinSpeed = (int)packet[3];
                }
                else if (packet[2] == 6) // max motor speed
                {
                    // MotorMaxSpeed = (int)packet[3];
                }
                else if (packet[2] == 7) // motor timeout
                {
                    int first = (int)packet[3];
                    int second = (int)packet[4];
                    //MotorTimeout = first*second;
                }
            }
            else if (packet[1] == 2) // rpi is requesting sensor data
            {
                int sensorType = (int)packet[2];
                GetSensorData(sensorType);
                byte header[4] = {0, 255, 0, sizeof(AllDataPacket)};
                Serial.write(header, sizeof(header));

                Serial.write(AllDataPacket, sizeof(AllDataPacket));
                setNewColors(2, 0, 0, 255, 0, 1);
            }
            else{
                // indicate a bad request
                setNewColors(4, 255, 0, 0, 0, 1);
            }
        }
        else // unknown header
        {
            setNewColors(0, 255, 0, 0, 0, 1); // set to red
            //SendResponse(3);
        }
        memset(RxPacket, 0, sizeof(RxPacket)); // clear out current packet
    }
}

// long MotorTimeout;
//send packet to its destination
void DigestPacket(byte packet[])
{
}

