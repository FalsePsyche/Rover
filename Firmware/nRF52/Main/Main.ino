
// TODO test with this line removed, because it should be in the MotionControl file, not in this file.
#include <Adafruit_MotorShield.h>

// #define TrigC 11 //10 //11
// #define EchoC 10 //11 //10
// #define MaxDist 400
// #define TrigR 13                       //8 //13
// #define EchoR 12                       //9 //12
// #define TrigL 9                        //12 //9
// #define EchoL 8                        //13 //8
// NewPing sonarC(TrigC, EchoC, MaxDist); // ID: 1
// NewPing sonarR(TrigR, EchoR, MaxDist); // ID: 2
// NewPing sonarL(TrigL, EchoL, MaxDist); // ID: 3
// int SonarWait = 20;                    // min delay between each sonar pings
//long SonarPrevPing = 0;
//long SonarThisPing = 0;

//bool ConnectFlag;
//bool listenState; // are we in listening mode?
////Global Packet Parameters
byte rawPacket[20]; // setting up raw packet var

long prevMillis = 0; // used in loop for elapsed time
long prevSec = 0;

long LoopWait = 30.00; //arduinoParms.WaitInterval;
long waitLong = 250;

///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma region Enumerations
// TODO everything in the region is not used.
// arduino parameters that can be modified by rpi
enum ArduinoParms // unused
{
    waitInterval,  // the interval in ms between the next serial read
    brightnessLED, // the level of brightness the LEDs light up at (0 - 255)
    serialTimeout  // unused
};

// the header of the message that is usually used to determine what the rest of is for and
//how long the rest of the message will be
enum MessageHeader
{
    error,      // = 0 //= 343.343
    warning,    // = 1 //= 2401.2401
    information // = 2 //117.117
};

enum MessagesInformation
{
    unknownHeader,
    unknownCommand
};
//Error Message:
// 343.343
// error's location of origin

#pragma endregion Enumerations

//diagnosis loop - DO NOT REMOVE
//void loop()
//{
//  byte diag[] = { 58, 2, 6 };
//  GetSensorData(6);
//  DigestPacket(diag);
//  //SendSensorDataToRPi(6);
//}

void Motor_Init();
void Sensor_Init();
void Comms_Init();
void LED_Init();

//will need to set up a system to import changed parms from rpi
void setup()
{
    // Serial.begin(9600); // start serial for output
    // Serial.println("Serial began");
    Wire.begin();

    // Init all the things
    LED_Init();
    // BootBlink();
    Motor_Init();
    // Sensor_Init();
    Comms_Init();
    BootBlink();
}
void Motor_SysTick();
void Sensor_SysTick();
void Comms_SysTick();
void UpdateLED();

void loop()
{
    unsigned long currentMillis = millis();

    // //only run loop after a certain amount of LoopWait time.
    if (currentMillis - prevMillis > LoopWait)
    {
        prevMillis = currentMillis;
        UpdateLED(); // update the led at a specific timing, this will act as the visual heartbeat of the system
    }
    // Sensor_SysTick();
    Comms_SysTick();
    Motor_SysTick();
}

// END FILE

