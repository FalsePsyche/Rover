/**
    LED control and logic.
*/

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <bluefruit.h>

#define LEDPIN 30 /* Pin used to drive the NeoPixels */
#define MATRIX_WIDTH 4
#define MATRIX_HEIGHT 8
#define LEDCOUNT 32
#define MATRIX_LAYOUT (NEO_MATRIX_TOP + NEO_MATRIX_RIGHT + NEO_MATRIX_COLUMNS + NEO_MATRIX_PROGRESSIVE)

Adafruit_NeoMatrix StatusPixel = Adafruit_NeoMatrix(MATRIX_WIDTH, MATRIX_HEIGHT, LEDPIN, MATRIX_LAYOUT, NEO_GRB + NEO_KHZ800);

int LEDBrightness = 10;

int LEDindicator = HIGH;

//init pixel color arrays, used to store the color value of each pixel
int Red[(MATRIX_HEIGHT * MATRIX_WIDTH)] = {0};
int Blue[(MATRIX_HEIGHT * MATRIX_WIDTH)] = {0};
int Green[(MATRIX_HEIGHT * MATRIX_WIDTH)] = {0};
int White[(MATRIX_HEIGHT * MATRIX_WIDTH)] = {0};

/*
    Initialize the LED control and logic.
*/
// LED control and logic initialization
extern void LED_Init()
{
    // set all values in pixel arrays to 0
    memset(Red, 0, sizeof(Red));
    memset(Blue, 0, sizeof(Red));
    memset(Green, 0, sizeof(Red));
    memset(White, 0, sizeof(Red));

    StatusPixel.begin();
    StatusPixel.setBrightness(LEDBrightness);
    StatusPixel.show();
}

// set new brightness level for all LEDs
extern void setNewBrightness(int brightness)
{
    LEDBrightness = brightness;
    StatusPixel.setBrightness(LEDBrightness);
    StatusPixel.show();
}

///Set new colors to the specified pixel
extern void setNewColors(int pxl, int red, int green, int blue, int white, int updateNow)
{
    Red[pxl] = red;
    Green[pxl] = green;
    Blue[pxl] = blue;
    White[pxl] = white;
    if (updateNow == 1)
    {
        // UpdateLED();
    }
}

extern void UpdateLED();

// update on board led to show loop is functional
void UpdateLED()
{
    if (LEDindicator == LOW)
    {
        for (int i = 0; i < (MATRIX_HEIGHT * MATRIX_WIDTH); i++)
        {
            StatusPixel.setPixelColor(i, Red[i], Green[i], Blue[i]); //, White[i]); // turn on
        }
        LEDindicator = HIGH;
    }
    else //if(LEDindicator == HIGH)
    {
        for (int i = 0; i < (MATRIX_HEIGHT * MATRIX_WIDTH); i++)
        {
            StatusPixel.setPixelColor(i, (Red[i] + 1) / 4, (Green[i] + 1) / 4,
                                      (Blue[i] + 1) / 4); //, (White[i] + 1) / 4); // lower brightness instead of completely turning off
                                                          // this is easier on the eyes
        }
        //StatusPixel.setPixelColor(0, ((Red+1)/4), (Green+1)/4, (Blue+1)/4); // turn off
        LEDindicator = LOW;
    }
    StatusPixel.show();
}

// this will make pixel zero blink 3 times when the init functions are complete executing.
void BootBlink()
{
    int delay1 = 100;
    int red = 255;
    int green = 255;
    int blue = 255;
    int white = 128;
    StatusPixel.setPixelColor(0, red, green, blue);
    StatusPixel.show();
    delay(delay1);
    StatusPixel.setPixelColor(0, 0, 0, 0);
    StatusPixel.show();
    delay(delay1);
    StatusPixel.setPixelColor(0, red, green, blue);
    StatusPixel.show();
    delay(delay1);
    StatusPixel.setPixelColor(0, 0, 0, 0);
    StatusPixel.show();
    delay(delay1);
    StatusPixel.setPixelColor(0, red, green, blue);
    StatusPixel.show();
    delay(delay1);
    StatusPixel.setPixelColor(0, 0, 0, 0);
    StatusPixel.show();
    delay(delay1);
}

// Can be used to show a red warning blink animation on pixel 0
void WarningBlink()
{
    int delay1 = 200;
    StatusPixel.setPixelColor(0, 255, 0, 0); //, white);
    StatusPixel.show();
    delay(delay1);
    StatusPixel.setPixelColor(0, 0, 0, 0); //, white);
    StatusPixel.show();
    delay(delay1);
    StatusPixel.setPixelColor(0, 255, 0, 0); //, white);
    StatusPixel.show();
    delay(delay1);
    StatusPixel.setPixelColor(0, 0, 0, 0); //, white);
    StatusPixel.show();
    delay(delay1);
    StatusPixel.setPixelColor(0, 255, 0, 0); //, white);
    StatusPixel.show();
    delay(delay1);
    StatusPixel.setPixelColor(0, 0, 0, 0); //, white);
    StatusPixel.show();
    delay(delay1);
}
#pragma endregion
///////////////////////////////////////////////////////////////////////////////////////////////////
