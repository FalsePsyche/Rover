
///////////////////////////////////////////////////////////////////////////////////////////////////

#define MotorShieldAddress 0x60
// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

Adafruit_DCMotor *Motor1 = AFMS.getMotor(1);
Adafruit_DCMotor *Motor2 = AFMS.getMotor(2);
Adafruit_DCMotor *Motor3 = AFMS.getMotor(3);
Adafruit_DCMotor *Motor4 = AFMS.getMotor(4);

int MotorMaxSpeed = 255;
int MotorMinSpeed = 30; // anything lower doesn't seem to work correctly on the adafruit motor shield v2
extern long MotorTimeout = 5000.00; // this is the # of seconds before motors stop if no command has been received
extern long MotorTimeoutPrevSec = 0.00;

//extern class Motion;
//
//class Motion
//{
//	//[2] Motor ID
//	//[3] Direction
//	//[4] Speed
//	//[5] Duration?
//public:

//move the rover
void MoveRover(byte packet[])
{
    int m1_direction = (int)packet[2]; // left side from bird's eye view
    int m1_speed = (int)packet[3];
    int m2_direction = (int)packet[4]; // right side from bird's eye view
    int m2_speed = (int)packet[5];
    int m3_direction = (int)packet[6];
    int m3_speed = (int)packet[7];
    int m4_direction = (int)packet[8];
    int m4_speed = (int)packet[9];
    SetMotorDir(m1_direction, m1_speed, m2_direction, m2_speed, m3_direction, m3_speed, m4_direction, m4_speed);
}

void StopRoverOnTimeout()
{
    Motor1->run(RELEASE);
    Motor2->run(RELEASE);
    Motor3->run(RELEASE);
    Motor4->run(RELEASE);
    Motor1->setSpeed(0);
    Motor2->setSpeed(0);
    Motor3->setSpeed(0);
    Motor4->setSpeed(0);
    setNewColors(1, 255, 0, 0, 0, 1); // set to error color indicator
}

void SetMotorDir(int m1_dir, int m1_speed, int m2_dir, int m2_speed, int m3_dir, int m3_speed, int m4_dir, int m4_speed)
{
    // get calculated motor speeds
    int m1speed = SetMotorSpeed(m1_speed);
    int m2speed = SetMotorSpeed(m2_speed);
    int m3speed = SetMotorSpeed(m3_speed);
    int m4speed = SetMotorSpeed(m4_speed);

    bool success = true;

    // if(m1_speed == 0 && m2_speed == 0 && m3_speed == 0 && m4_speed == 0){
    //     setNewColors(1, 0, 0, 255, 0, 1);
    // }

    // Motor 1
    switch (m1_dir)
    {
    case 0: // stop motor 1
        Motor1->run(RELEASE);
        Motor1->setSpeed(0);
        break;
    case 1: // motor 1 backward
        Motor1->run(RELEASE);
        Motor1->run(BACKWARD);
        Motor1->setSpeed(m1speed);
        break;
    case 2: // motor 1 forward
        Motor1->run(RELEASE);
        Motor1->run(FORWARD);
        Motor1->setSpeed(m1speed);
        break;
    case 3: // motor 1 unchanged
        // do nothing
        break;
    default:
        // setNewColors(1, 255, 0, 0, 0, 1);
        success = false;
        break;
    }
    // Motor 2
    switch (m2_dir)
    {
    case 0: // stop motor 2
        Motor2->run(RELEASE);
        Motor2->setSpeed(0);
        break;
    case 1: // motor 2 backward
        Motor2->run(RELEASE);
        Motor2->run(BACKWARD);
        Motor2->setSpeed(m2speed);
        break;
    case 2: // motor 2 forward
        Motor2->run(RELEASE);
        Motor2->run(FORWARD);
        Motor2->setSpeed(m2speed);
        break;
    case 3: // motor 2 unchanged
        // do nothing
        break;
    default:
        // setNewColors(1, 255, 0, 0, 0, 1);
        success = false;
        break;
    }
    // Motor 3
    switch (m3_dir)
    {
    case 0: // stop motor 3
        Motor3->run(RELEASE);
        Motor3->setSpeed(0);
        break;
    case 1: // motor 3 backward
        Motor3->run(RELEASE);
        Motor3->run(BACKWARD);
        Motor3->setSpeed(m3speed);
        break;
    case 2: // motor 3 forward
        Motor3->run(RELEASE);
        Motor3->run(FORWARD);
        Motor3->setSpeed(m3speed);
        break;
    case 3: // motor 3 unchanged
        // do nothing
        break;
    default:
        // setNewColors(1, 255, 0, 0, 0, 1);
        success = false;
        break;
    }
    // Motor 4
    switch (m4_dir)
    {
    case 0: // stop motor 4
        Motor4->run(RELEASE);
        Motor4->setSpeed(0);
        break;
    case 1: // motor 4 backward
        Motor4->run(RELEASE);
        Motor4->run(BACKWARD);
        Motor4->setSpeed(m4speed);
        break;
    case 2: // motor 4 forward
        Motor4->run(RELEASE);
        Motor4->run(FORWARD);
        Motor4->setSpeed(m4speed);
        break;
    case 3: // motor 4 unchanged
        // do nothing
        break;
    default:
        // setNewColors(1, 255, 0, 0, 0, 1);
        success = false;
        break;
    }
    if (success)
    {
        setNewColors(1, 0, 255, 0, 0, 1); // set to green to indicate a new motor command has arrived
    }
    else
    {
        setNewColors(1, 255, 0, 0, 0, 1); // set to red to indicate bad
    }
}

// sets both motor speeds. maybe one day this will allow for independent motor speed control.
int SetMotorSpeed(int speed)
{
    //    setNewColors(1, 0, 0, 255, 0, 1);
    double speedPercent = (double)speed / 100.00;                 // calc packet's speed command to a %
    int speedRange = MotorMaxSpeed - MotorMinSpeed;               // calc usable speed range
    int motorSpeed = (speedPercent * speedRange) + MotorMinSpeed; // calc actual motor speed

    // return motor speed
    return motorSpeed;
}
// };

extern void Motor_Init();

void Motor_Init()
{
    AFMS.begin(); // create with the default frequency 1.6KHz
    ////AFMS.begin(1000);  // OR with a different frequency, say 1KHz

    // let rover stretch
    StopRoverOnTimeout();
    SetMotorDir(1, 255, 1, 255, 1, 255, 1, 255);
    delay(1000);
    StopRoverOnTimeout();
    setNewColors(1, 0, 0, 255, 0, 1);
}

extern void Motor_SysTick(void);
// long prevMillis = 0;
void Motor_SysTick(void)
{
    unsigned long currentMillis = millis();
    // check for  new command for motors
    if (MotorCommand[0] == 0) // this never seems to be true
    {
        // TODO this doesn't seem to work as expected
        if (currentMillis - prevMillis > 1000)
        {
            prevMillis = currentMillis;
            setNewColors(1, 0, 0, 255, 0, 1); // set to blue to indicate idle, we have not received a motor command for the last second

            // if(CurrentState == MOVING)
            // {
            //     // StopRoverOnTimeout();
            // }
        }
    }
    else if (MotorCommand[1] == 0) // this never seems to be true
    {
        setNewColors(1, 0, 0, 255, 0, 1); // set to blue to indicate idle, we have not received a motor command for the last second
    }
    else
    {
        // execute new command
        MoveRover(MotorCommand);
        memset(MotorCommand, 0, sizeof(MotorCommand)); // clear command from memory, to not repeat a completed command.
        // CurrentState = MOVING;
        prevMillis = currentMillis; // reset idle check
    }
}

