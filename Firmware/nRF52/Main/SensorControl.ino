

#include <Adafruit_BMP085_U.h>

#include <Adafruit_Sensor.h>

#include <Adafruit_LSM303_U.h>
// #include <Adafruit_LSM303.h>

// #include </Adafruit_LSM303DLHC/Adafruit_LSM303_U.h>
// #include Adafruit_BMP085_U
#include <Adafruit_L3GD20_U.h>
// #include <Adafruit_L3GD20.h>

#pragma region Compass
typedef struct CompassStruct
{
    float header; // 10
    float X;
    float Y;
    float Z;
    /*float heading;
	float pitch;
	float roll;*/
};

typedef union CompassPacket {
    CompassStruct compass;
    byte I2CPacket[sizeof(CompassStruct)];
};
#pragma endregion Compass
/////////////////////////////////////////////////
// Byte size: int, float(x6)
#pragma region Gyro
typedef struct GyroStruct
{
    float header; // 20
    float X;
    float Y;
    float Z;
    /*float heading;
	float pitch;
	float roll;*/
};

typedef union GyroPacket {
    GyroStruct gyro;
    byte I2CPacket[sizeof(GyroStruct)];
};
#pragma endregion Gyro
//////////////////////////////////////////////////
// Byte size: int, float(x6)
#pragma region Accelerometer
typedef struct AccelStruct
{
    float header; // 30
    float X;
    float Y;
    float Z;
    /*float heading;
	float pitch;
	float roll;*/
};

typedef union AccelPacket {
    AccelStruct accel;
    byte I2CPacket[sizeof(AccelStruct)];
};
#pragma endregion Accelerometer
//////////////////////////////////////////////////
// Byte size: int, float(x2)
#pragma region Temperature &Pressure
typedef struct TempPressStruct
{
    float header; // 40
                  //hectoPascal
    float hPa;
    //Celsius
    float C;
};

typedef union TempPressPacket {
    TempPressStruct temppress;
    byte I2CPacket[sizeof(TempPressStruct)];
};
#pragma endregion Temperature &Pressure
//////////////////////////////////////////////////
#pragma region Pressure
//typedef struct PressureStruct
//{
//  int header; // 50
//  //hectoPascal
//  float hPa;
//  //int endMsg;
//};
//
//typedef union PressurePacket {
//  PressureStruct pressure;
//  byte I2CPacket[sizeof(PressureStruct)];
//};
#pragma endregion Pressure DEPRECIATED
//////////////////////////////////////////////////
// Byte size: int, float(x11)
#pragma region 10DOF
//the whole shebang
typedef struct tenDOF
{
    float header; // 50
    float compX;
    float compY;
    float compZ;

    float gyroX;
    float gyroY;
    float gyroZ;

    float accelX;
    float accelY;
    float accelZ;

    float tempC;

    float pressure_hPa;
};

typedef union tenDOFPacket {
    tenDOF dof;
    byte I2CPacket[sizeof(tenDOF)];
};
#pragma endregion 10DOF
///////// HC SR04 Sensors /////////////////////////
// Byte size: int, float(x3)
#pragma region USS
typedef struct SonarStruct
{
    float header; // 60
    float SonarC;
    float SonarR;
    float SonarL;
};

typedef union SonarPacket {
    SonarStruct sonarstruct;
    byte I2CPacket[sizeof(SonarStruct)];
};
#pragma endregion USS
//////////////////////////////////////////////////
// Byte size: int, float(x14)
#pragma region All Data Types
//the whole shebang plus a few
typedef struct allData
{
    float header; // 70
    float compX;
    float compY;
    float compZ;

    float gyroX;
    float gyroY;
    float gyroZ;

    float accelX;
    float accelY;
    float accelZ;

    float tempC;
    float pressure_hPa;

    float SonarC;
    float SonarR;
    float SonarL;

    float EOL; // 200.200
};

typedef union allDataPacket {
    allData all;
    byte I2CPacket[sizeof(allData)];
};
// extern allDataPacket;
#pragma endregion All Data Types
//////////////////////////////////////////////////
CompassPacket compPacket; // 1

GyroPacket gyroPacket; // 2

AccelPacket accelPacket; // 3

TempPressPacket temppressPacket; // 4

//PressurePacket pressurePacket; // 5 - has been combined with temp

tenDOFPacket dofpacket; // 5

SonarPacket sonarpacket; // 6 - all 3 sonar sensors

allDataPacket allpacket; // 7 - all data (10DOF and 3 sonar sensors)
                         //////////////////////////////////////////////////

//removing becuase its unused
//Ping all 3 sonar sensors at the same time.
//void Ping()
//{
//	sonarpacket.sonarstruct.header = 60;
//
//	unsigned int uSc = sonarC.ping();
//	sonarpacket.sonarstruct.SonarC = (float)uSc / US_ROUNDTRIP_CM;
//	delay(SonarWait);//time to wait inbetween sensor readings (to lower interference and feedback)
//
//	unsigned int uSr = sonarR.ping();
//	sonarpacket.sonarstruct.SonarR = (float)uSr / US_ROUNDTRIP_CM;
//	delay(SonarWait);
//
//	unsigned int uSl = sonarL.ping();
//	sonarpacket.sonarstruct.SonarL = (float)uSl / US_ROUNDTRIP_CM;
//}

// #pragma endregion Sensor Data

Adafruit_LSM303_Mag_Unified mag = Adafruit_LSM303_Mag_Unified(1);
Adafruit_L3GD20_Unified gyro = Adafruit_L3GD20_Unified(2);
Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(3);
Adafruit_BMP085_Unified bmp = Adafruit_BMP085_Unified(4);

extern void Sensor_Init();

void Sensor_Init()
{
    mag.enableAutoRange(true);
    accel.enableAutoRange(true);
    gyro.enableAutoRange(true);
    bmp.enableAutoRange(true);

    /* Initialize the sensor */
    // if (!mag.begin() || !accel.begin() || !gyro.begin() || !bmp.begin()) 
    // {
    //     /* There was a problem detecting the LSM303 ... check your connections */
    //     Serial.println("Ooops, a sensor was not detected ... Check your wiring!");
    //     WarningBlink();
    // }

    // The gyro begin check causes the device to freeze at the begin call when the sensor breakout is not plugged in. But if the sensor breakout is plugged in then it freezes on the accel begin call. [said tanner on Sun, Jun 25, 2017 at T13:27:36 -5:00]
    // thus the sensor breakout board is dead.[said tanner on Sun, Jun 25, 2017 at T13:27:51 -5:00]
    if (!accel.begin())
    {
        setNewColors(4, 255, 0, 0, 0, 1);
    }
    BootBlink();
    if (!gyro.begin())
    {
        setNewColors(5, 255, 0, 0, 0, 1);
    }
    BootBlink();
    if (!mag.begin())
    {
        setNewColors(6, 255, 0, 0, 0, 1);
    }
    BootBlink();
    if (!bmp.begin())
    {
        setNewColors(7, 255, 0, 0, 0, 1);
    }
    BootBlink();
    setNewColors(2, 0, 0, 255, 0, 1);
}

extern void Sensor_SysTick();

void Sensor_SysTick()
{
    // todo check all sensors
    GetSensorData(7);
    // todo store sensor data
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// byte AllDataPacket[30];
// initializes sensor event and adds the data to a struct
void GetSensorData(int sensorType = 7)
{
    /* Get a new sensor event */
    sensors_event_t event;

    switch (sensorType)
    {
    case 0:
        //error handling here
        break;
    case 1:
        mag.getEvent(&event);
        compPacket.compass.header = 10;
        //this is working correctly
        compPacket.compass.X = event.magnetic.x;
        compPacket.compass.Y = event.magnetic.y;
        compPacket.compass.Z = event.magnetic.z;
        /*compPacket.compass.heading = event.magnetic.heading;
		compPacket.compass.pitch = event.magnetic.pitch;
		compPacket.compass.roll = event.magnetic.roll;*/
        break;
    case 2:
        gyro.getEvent(&event);
        gyroPacket.gyro.header = 20;
        gyroPacket.gyro.X = event.gyro.x;
        gyroPacket.gyro.Y = event.gyro.y;
        gyroPacket.gyro.Z = event.gyro.z;
        /*gyroPacket.gyro.heading = event.gyro.heading;
		gyroPacket.gyro.pitch = event.gyro.pitch;
		gyroPacket.gyro.roll = event.gyro.roll;*/
        break;
    case 3:
        accel.getEvent(&event);
        accelPacket.accel.header = 30;
        accelPacket.accel.X = event.acceleration.x;
        accelPacket.accel.Y = event.acceleration.y;
        accelPacket.accel.Z = event.acceleration.z;
        /*accelPacket.accel.heading = event.acceleration.heading;
		accelPacket.accel.pitch = event.acceleration.pitch;
		accelPacket.accel.roll = event.acceleration.roll;*/
        break;
    case 4:
        bmp.getEvent(&event);
        temppressPacket.temppress.header = 40;
        temppressPacket.temppress.hPa = event.pressure;
        temppressPacket.temppress.C = event.temperature;
        break;
    case 5: // do all sensors
        mag.getEvent(&event);
        dofpacket.dof.header = 50;
        //TODO ACTUAllY i need to make a struct byte array combo specific to all of these
        dofpacket.dof.compX = event.magnetic.x;
        dofpacket.dof.compY = event.magnetic.y;
        dofpacket.dof.compZ = event.magnetic.z;

        gyro.getEvent(&event);
        dofpacket.dof.gyroX = event.gyro.x;
        dofpacket.dof.gyroY = event.gyro.y;
        dofpacket.dof.gyroZ = event.gyro.z;

        accel.getEvent(&event);
        dofpacket.dof.accelX = event.acceleration.x;
        dofpacket.dof.accelY = event.acceleration.y;
        dofpacket.dof.accelZ = event.acceleration.z;

        bmp.getEvent(&event);
        dofpacket.dof.tempC = event.temperature;
        dofpacket.dof.pressure_hPa = event.pressure;
        break;
    // case 6:
    //Ping();
    // allpacket.all.header = 60;

    // unsigned int uSc = sonarC.ping();
    // allpacket.all.SonarC = (float)uSc / US_ROUNDTRIP_CM;
    // delay(SonarWait); //time to wait inbetween sensor readings (to lower interference and feedback, maybe?)

    // unsigned int uSr = sonarR.ping();
    // allpacket.all.SonarR = (float)uSr / US_ROUNDTRIP_CM;
    // delay(SonarWait);

    // unsigned int uSl = sonarL.ping();
    // allpacket.all.SonarL = (float)uSl / US_ROUNDTRIP_CM;
    // break;
    case 7: // do all of the things

        allpacket.all.header = HEADER;

        mag.getEvent(&event);
        allpacket.all.compX = event.magnetic.x;
        allpacket.all.compY = event.magnetic.y;
        allpacket.all.compZ = event.magnetic.z;

        gyro.getEvent(&event);
        allpacket.all.gyroX = event.gyro.x;
        allpacket.all.gyroY = event.gyro.y;
        allpacket.all.gyroZ = event.gyro.z;

        accel.getEvent(&event);
        allpacket.all.accelX = event.acceleration.x;
        allpacket.all.accelY = event.acceleration.y;
        allpacket.all.accelZ = event.acceleration.z;

        bmp.getEvent(&event);
        allpacket.all.tempC = event.temperature;
        allpacket.all.pressure_hPa = event.pressure;

        // unsigned int uSc = sonarC.ping();
        allpacket.all.SonarC = 0; //(float)uSc / US_ROUNDTRIP_CM;
        // delay(SonarWait); //time to wait inbetween sensor readings (to lower interference and feedback, maybe?)

        // unsigned int uSr = sonarR.ping();
        allpacket.all.SonarR = 0; //(float)uSr / US_ROUNDTRIP_CM;
        // delay(SonarWait);

        // unsigned int uSl = sonarL.ping();
        allpacket.all.SonarL = 0; //(float)uSl / US_ROUNDTRIP_CM;

        allpacket.all.EOL = 200;

        break;
    }

    //creates a byte array for each sensor and then write it through serial to rpi
    //get all the sizes up here since damn compiler doesn't like them inside the case:
    // int compassSize = sizeof(compPacket.compass);
    // int gyroSize = sizeof(gyroPacket.gyro);
    // int accelSize = sizeof(accelPacket.accel);
    // int tempSize = sizeof(temppressPacket.temppress);
    // int dofsize = sizeof(dofpacket.dof);
    // int sonarSize = sizeof(sonarpacket.sonarstruct);
    // int alldataSize = sizeof(allpacket.all);
    memcpy(AllDataPacket, allpacket.I2CPacket, sizeof(allpacket.all)); // pass the new sensor data to the global data item
    // AllDataPacket allpacket.I2CPacket
    // switch (sensorType)
    // {
    // case 1: // compass
    //     Serial.write(compPacket.I2CPacket, compassSize);
    //     break;
    // case 2: // gyro
    //     Serial.write(gyroPacket.I2CPacket, gyroSize);
    //     break;
    // case 3: // accel
    //     Serial.write(accelPacket.I2CPacket, accelSize);
    //     break;
    // case 4: // temp and pressure
    //     Serial.write(temppressPacket.I2CPacket, tempSize);
    //     break;
    // case 5: // all 10DOF data
    //     Serial.write(dofpacket.I2CPacket, dofsize);
    //     break;
    // case 6: // sonar
    //     Serial.write(sonarpacket.I2CPacket, sonarSize);
    //     break;
    // case 7:
    //     Serial.write(allpacket.I2CPacket, alldataSize);
    //     break;
    // }
}
