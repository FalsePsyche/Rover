#ifndef _INP_Input_H_
#define _INP_Input_H_

#include <Arduino.h> //needed for Serial.println

//#include <Wire.h>


#define Y_MOTION_PIN (A0)
#define X_MOTION_PIN (A1)
#define ROTATE_PIN   (A2)

class Input
{
  protected:
    int _X_value; // most recent Y value
    int _Y_value;
    int _Rotate_value;

  private:
    // uint8_t mvToPercent (float mvolts);

  public:
    // Constructor
    Input(void)
    {
      _X_value            = 0;
      _Y_value            = 0;
      _Rotate_value       = 0;
    }

    int     ReadMotionFR               (void);
    int     ReadMotionLR               (void);
    int     ReadRotate                 (void);
    // int ReadUD (void);
    void UpdateAll (void);


};

#endif