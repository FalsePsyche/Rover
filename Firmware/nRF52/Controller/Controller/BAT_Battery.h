
#ifndef _BAT_Battery_H_
#define _BAT_Battery_H_

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define VBAT_PIN (A7)
#define VBAT_MV_PER_LSB (0.73242188F) // 3.0V ADC range and 12-bit ADC resolution = 3000mV/4096
#define VBAT_DIVIDER (0.71275837F)    // 2M + 0.806M voltage divider on VBAT = (2M / (0.806M + 2M))
#define VBAT_DIVIDER_COMP (1.403F)    // Compensation factor for the VBAT divider

class Battery  : public Adafruit_SSD1306
{
  protected:
    float _battery;
    bool  _batteryIcon;
    bool  _batteryVisible;

  private:
    uint8_t mvToPercent (float mvolts);

  public:
    // Constructor
    Battery(void)
    {
      _battery            = 0.0F;
      _batteryIcon        = true;
      _batteryVisible     = true;
    }

    int     readVBAT               (void);

    void    setBattery             ( float vbat   )     { _battery        = vbat;   }
    void    setBatteryVisible      ( bool  enable )     { _batteryVisible = enable; }
    void    setBatteryIcon         ( bool  enable )     { _batteryIcon    = enable; }

    float   GetBatteryMilliVoltage (void);
    uint8_t GetBatteryPercentage   (void);
    void    RenderIcon             (void);

};

#endif /* _Battery_H_ */