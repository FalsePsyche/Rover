#include "BAT_Battery.h"

uint8_t Battery::mvToPercent(float mvolts)
{
    uint8_t battery_level;

    if (mvolts >= 3000)
    {
        battery_level = 100;
    }
    else if (mvolts > 2900)
    {
        battery_level = 100 - ((3000 - mvolts) * 58) / 100;
    }
    else if (mvolts > 2740)
    {
        battery_level = 42 - ((2900 - mvolts) * 24) / 160;
    }
    else if (mvolts > 2440)
    {
        battery_level = 18 - ((2740 - mvolts) * 12) / 300;
    }
    else if (mvolts > 2100)
    {
        battery_level = 6 - ((2440 - mvolts) * 6) / 340;
    }
    else
    {
        battery_level = 0;
    }

    return battery_level;
}

int Battery::readVBAT(void)
{
    int raw;

    // Set the analog reference to 3.0V (default = 3.6V)
    analogReference(AR_INTERNAL_3_0);

    // Set the resolution to 12-bit (0..4095)
    analogReadResolution(12); // Can be 8, 10, 12 or 14

    // Let the ADC settle
    delay(1);

    // Get the raw 12-bit, 0..3000mV ADC value
    raw = analogRead(VBAT_PIN);

    // Set the ADC back to the default settings
    analogReference(AR_DEFAULT);
    analogReadResolution(10);

    return raw;
}

float Battery::GetBatteryMilliVoltage(void)
{
    // Get a raw ADC reading
    int vbat_raw = readVBAT();

    // Convert the raw value to compensated mv, taking the resistor-
    // divider into account (providing the actual LIPO voltage)
    // ADC range is 0..3000mV and resolution is 12-bit (0..4095),
    // VBAT voltage divider is 2M + 0.806M, which needs to be added back
    float vbat_mv = (float)vbat_raw * VBAT_MV_PER_LSB * VBAT_DIVIDER_COMP;

    return vbat_mv;
}

uint8_t Battery::GetBatteryPercentage(void)
{
    // Get a raw ADC reading
    int vbat_raw = readVBAT();

    // Convert from raw mv to percentage (based on LIPO chemistry)
    uint8_t vbat_per = mvToPercent(vbat_raw * VBAT_MV_PER_LSB);
    return vbat_per;
}

/******************************************************************************/
/*!
    @brief  Renders the battery icon
*/
/******************************************************************************/
void Battery::RenderIcon(void)
{
#define BATTTEXT_STARTX 77
#define BATTTEXT_STARTY 0
#define BATTICON_STARTX 110
#define BATTICON_STARTY 0
#define BATTICON_WIDTH 18
#define BATTICON_BARWIDTH3 ((BATTICON_WIDTH - 6) / 3)

    //_battery = GetBatteryMilliVoltage() / 1000.00;
    _battery = GetBatteryPercentage();

    if (_batteryVisible)
    {
        // Render the voltage in text
        // setCursor(BATTTEXT_STARTX, BATTTEXT_STARTY);
        // print(_battery, 2);
        // println("V");

        // Render the battery icon if requested
        if (_batteryIcon)
        {
            // Draw the base of the battery
            drawLine(BATTICON_STARTX + 1,
                     BATTICON_STARTY,
                     BATTICON_STARTX + BATTICON_WIDTH - 4,
                     BATTICON_STARTY,
                     WHITE);
            drawLine(BATTICON_STARTX,
                     BATTICON_STARTY + 1,
                     BATTICON_STARTX,
                     BATTICON_STARTY + 5,
                     WHITE);
            drawLine(BATTICON_STARTX + 1,
                     BATTICON_STARTY + 6,
                     BATTICON_STARTX + BATTICON_WIDTH - 4,
                     BATTICON_STARTY + 6,
                     WHITE);
            drawPixel(BATTICON_STARTX + BATTICON_WIDTH - 3,
                      BATTICON_STARTY + 1,
                      WHITE);
            drawPixel(BATTICON_STARTX + BATTICON_WIDTH - 2,
                      BATTICON_STARTY + 1,
                      WHITE);
            drawLine(BATTICON_STARTX + BATTICON_WIDTH - 1,
                     BATTICON_STARTY + 2,
                     BATTICON_STARTX + BATTICON_WIDTH - 1,
                     BATTICON_STARTY + 4,
                     WHITE);
            drawPixel(BATTICON_STARTX + BATTICON_WIDTH - 2,
                      BATTICON_STARTY + 5,
                      WHITE);
            drawPixel(BATTICON_STARTX + BATTICON_WIDTH - 3,
                      BATTICON_STARTY + 5,
                      WHITE);
            drawPixel(BATTICON_STARTX + BATTICON_WIDTH - 3,
                      BATTICON_STARTY + 6,
                      WHITE);

            // Draw the appropriate number of bars
            // if (_battery > 4.26F)
            if (_battery > 75)
            {
                // USB (Solid Rectangle)
                fillRect(BATTICON_STARTX + 2,    // X
                         BATTICON_STARTY + 2,    // Y
                         BATTICON_BARWIDTH3 * 3, // W
                         3,                      // H
                         WHITE);
            }
            else if ((_battery <= 75) && (_battery >= 50))
            {
                // Three bars
                for (uint8_t i = 0; i < 3; i++)
                {
                    fillRect(BATTICON_STARTX + 2 + (i * BATTICON_BARWIDTH3),
                             BATTICON_STARTY + 2,
                             BATTICON_BARWIDTH3 - 1,
                             3,
                             WHITE);
                }
            }
            else if ((_battery < 50) && (_battery >= 25))
            {
                // Two bars
                for (uint8_t i = 0; i < 2; i++)
                {
                    fillRect(BATTICON_STARTX + 2 + (i * BATTICON_BARWIDTH3),
                             BATTICON_STARTY + 2,
                             BATTICON_BARWIDTH3 - 1,
                             3,
                             WHITE);
                }
            }
            else if ((_battery < 25) && (_battery >= 0))
            {
                // One bar
                fillRect(BATTICON_STARTX + 2,
                         BATTICON_STARTY + 2,
                         BATTICON_BARWIDTH3 - 1,
                         3,
                         WHITE);
            }
            else
            {
                // No bars
            }
        }
    }
}