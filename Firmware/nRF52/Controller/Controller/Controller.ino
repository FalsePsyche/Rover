/*********************************************************************
 This is an example for our nRF52 based Bluefruit LE modules
 Pick one up today in the adafruit shop!
 Adafruit invests time and resources providing this open source code,
 please support Adafruit and open-source hardware by purchasing
 products from Adafruit!
 MIT license, check LICENSE for more information
 All text above, and the splash screen below must be included in
 any redistribution
*********************************************************************/

/* This sketch demonstrates the client Current Time Service using the
 * BLEClientCts API(). After uploading, go to iOS setting and connect
 * to Bluefruit52, and then press PAIR.
 * 
 * Note: Currently only iOS act as a CTS server, Android does not. The
 * easiest way to test this sketch is using an iOS device.
 * 
 * This sketch is similar to client_cts but also uses a Feather OLED Wing
 * to display time https://www.adafruit.com/product/2900
 *
 * Current Time Service info:
 *   https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.service.current_time.xml
 *   https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.characteristic.current_time.xml
 *   https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.characteristic.local_time_information.xml
 */

#include <bluefruit.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "BAT_Battery.h"
#include "INP_Input.h"

#define OLED_RESET 4
#define YAXIS 0 // the line the yaxis data will be printed on
#define XAXIS 8

Adafruit_SSD1306 oled(OLED_RESET);
Battery battery = Battery();
Input input = Input();

BLEClientDis clientDis;
BLEClientUart clientUart;

void setup()
{
    Serial.begin(115200);

    Serial.println("Bluefruit52 Central BLEUART Example");
    Serial.println("-----------------------------------");

    // init with the I2C addr 0x3C (for the 128x32) and show splashscreen
    oled.begin(SSD1306_SWITCHCAPVCC, 0x3C);
    oled.display();

    oled.setTextSize(1); // max is 4 line, 21 chars each
    oled.setTextColor(WHITE);

    // splash screen effect
    delay(1000);
    oled.clearDisplay();
    oled.display();
    analogReadResolution(12);

    // Enable both peripheral and central
    Bluefruit.begin(true, true);
    Bluefruit.setName("Bluefruit52");

    // Configure DIS client
    clientDis.begin();

    // Init BLE Central Uart Serivce
    clientUart.begin();
    clientUart.setRxCallback(uart_rx_callback);

    // Increase Blink rate to different from PrPh advertising mode
    Bluefruit.setConnLedInterval(250);

    // Callbacks for Central
    Bluefruit.Central.setConnectCallback(connect_callback);
    Bluefruit.Central.setDisconnectCallback(disconnect_callback);

    // Start Central Scan
    Bluefruit.Central.setScanCallback(scan_callback);
    Bluefruit.Central.startScanning();
}

/**
 * Callback invoked when scanner pick up an advertising data
 * @param report Structural advertising data
 */
void scan_callback(ble_gap_evt_adv_report_t *report)
{
    // Check if advertising contain BleUart service
    if (Bluefruit.Central.checkUuidInScan(report, BLEUART_UUID_SERVICE))
    {
        write("Attemping connection...");

        // Connect to device with bleuart service in advertising
        // Use Min & Max Connection Interval default value
        Bluefruit.Central.connect(report);
    }
}

/**
 * Callback invoked when an connection is established
 * @param conn_handle
 */
void connect_callback(uint16_t conn_handle)
{
    write("Connecting...");
    // Serial.print("Dicovering DIS ... ");
    if (clientDis.discover(conn_handle))
    {
        // Serial.println("Found it");
        char buffer[32 + 1];

        // read and print out Manufacturer
        memset(buffer, 0, sizeof(buffer));
        if (clientDis.getManufacturer(buffer, sizeof(buffer)))
        {
            // Serial.print("Manufacturer: ");
            // Serial.println(buffer);
        }

        // read and print out Model Number
        memset(buffer, 0, sizeof(buffer));
        if (clientDis.getModel(buffer, sizeof(buffer)))
        {
            // Serial.print("Model: ");
            // Serial.println(buffer);
        }

        // Serial.println();
    }

    // Serial.print("Discovering BLE Uart Service ... ");

    if (clientUart.discover(conn_handle))
    {
        // Serial.println("Found it");

        // Serial.println("Enable TXD's notify");
        clientUart.enableTXD();

        // Serial.println("Ready to receive from peripheral");
        write("Connected");
    }
    else
    {
        // Serial.println("Found NONE");
        write("Unable to connect");
    }
}

/**
 * Callback invoked when a connection is dropped
 * @param conn_handle
 * @param reason
 */
void disconnect_callback(uint16_t conn_handle, uint8_t reason)
{
    (void)conn_handle;
    (void)reason;

    // Serial.println("Disconnected");
    // Serial.println("Bluefruit will auto start scanning (default)");
    oled.clearDisplay();
    oled.println("Disconnected");
    oled.display();
}

void uart_rx_callback(void)
{
    Serial.print("[RX]: ");

    while (clientUart.available())
    {
        Serial.print((char)clientUart.read());
    }

    Serial.println();
}

void loop()
{
    // update the battery icon
    battery.RenderIcon();
    oled.setCursor(0, 0);
    // input.UpdateAll();
    // oled.println(input.ReadMotionFR());
    // oled.println(input.ReadMotionLR());
    // oled.println(input.ReadRotate());
    oled.display();
    // // ellipse();
    delay(50);
    // oled.clearDisplay();

    if (Bluefruit.Central.connected())
    {
        // Not discovered yet
        if (clientUart.discovered())
        {
            // Discovered means in working state
            // Get Serial input and send to Peripheral
            if (Serial.available())
            {
                delay(2); // delay a bit for all characters to arrive

                char str[20 + 1] = {0};
                Serial.readBytes(str, 20);

                clientUart.print(str);
            }
        }
    }
}

void write(char* words)
{
    oled.clearDisplay();
    oled.setCursor(0, 0);
    oled.println(words);
    oled.display();
    delay(500);
}