#include "INP_Input.h"

int Input::ReadMotionFR(void)
{
    _X_value = analogRead(A1);
    return _X_value;
}

int Input::ReadMotionLR(void)
{
    _Y_value = analogRead(Y_MOTION_PIN);
    return _Y_value;
}

int Input::ReadRotate(void)
{
    _Rotate_value = analogRead(ROTATE_PIN); 
    return _Rotate_value;
}

// int Input::ReadUD(void)
// {
//     return analogRead(A3);
// }


void Input::UpdateAll(void)
{
    ReadMotionFR();
    ReadMotionLR();
    ReadRotate();
    // ReadUD();
}