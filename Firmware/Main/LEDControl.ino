
#define LEDPIN 15

int LEDBrightness = 150;

// int ledState = LOW;

int LEDindicator = HIGH;

Adafruit_NeoPixel StatusPixel = Adafruit_NeoPixel(32, 30, NEO_GRBW + NEO_KHZ800);

extern void LED_Init()
{
    //set up status LEDs
    // pinMode(LEDPIN, OUTPUT);
    // digitalWrite(LEDPIN, LOW); // update LED
    StatusPixel.begin();
    StatusPixel.show();
    StatusPixel.setBrightness(LEDBrightness);
}

extern void setNewBrightness(int brightness)
{
    LEDBrightness = brightness;
    StatusPixel.setBrightness(LEDBrightness);
    StatusPixel.show();    
}

extern void setNewColors(int pxl, int red, int green, int blue, int white, int updateNow)
{
	Red[pxl] = red;
	Green[pxl] = green;
	Blue[pxl] = blue;
	White[pxl] = white;
	if (updateNow == 1)
	{
		// UpdateLED();
	}
}

extern void UpdateLED();

// update on board led to show loop is functional
void UpdateLED()
{
	if (LEDindicator == LOW)
	{
		for (int i = 0; i < 7; i++)
		{
			StatusPixel.setPixelColor(i, Red[i], Green[i], Blue[i], White[i]); // turn on
		}

		LEDindicator = HIGH;
		StatusPixel.show();
	}
	else //if(LEDindicator == HIGH)
	{
		for (int i = 0; i < 7; i++)
		{
			StatusPixel.setPixelColor(i, (Red[i] + 1) / 4, (Green[i] + 1) / 4,
				(Blue[i] + 1) / 4, (White[i] + 1) / 4); // lower brightness instead of completely turning off
														// this is easier on the eyes
		}
		//StatusPixel.setPixelColor(0, ((Red+1)/4), (Green+1)/4, (Blue+1)/4); // turn off
		LEDindicator = LOW;
		StatusPixel.show();
	}

	StatusPixel.show();
	// digitalWrite(LEDPIN, LEDindicator); // update LED
}

//this should cause the arduino L LED to blink steadily for 3 seconds on boot.
void BootBlink()
{
	int delay1 = 100;
	int red = 255;
	int green = 255;
	int blue = 255;
	int white = 128;
	StatusPixel.setPixelColor(0, red, green, blue, white);
	StatusPixel.show();
	// digitalWrite(LEDPIN, HIGH); // update LED
	delay(delay1);
	StatusPixel.setPixelColor(0, 0, 0, 0);
	StatusPixel.show();
	// digitalWrite(LEDPIN, LOW); // update LED
	delay(delay1);
	StatusPixel.setPixelColor(0, red, green, blue, white);
	StatusPixel.show();
	// digitalWrite(LEDPIN, HIGH); // update LED
	delay(delay1);
	StatusPixel.setPixelColor(0, 0, 0, 0);
	StatusPixel.show();
	// digitalWrite(LEDPIN, LOW); // update LED
	delay(delay1);
	StatusPixel.setPixelColor(0, red, green, blue, white);
	StatusPixel.show();
	// digitalWrite(LEDPIN, HIGH); // update LED
	delay(delay1);
	StatusPixel.setPixelColor(0, 0, 0, 0);
	StatusPixel.show();
	// digitalWrite(LEDPIN, LOW); // update LED
	delay(delay1);
}

void WarningBlink()
{
	int delay1 = 200;
	StatusPixel.setPixelColor(0, 255, 0, 0, 0);
	StatusPixel.show();
	// digitalWrite(LEDPIN, HIGH); // update LED
	delay(delay1);
	StatusPixel.setPixelColor(0, 0, 0, 0, 0);
	StatusPixel.show();
	// digitalWrite(LEDPIN, LOW); // update LED
	delay(delay1);
	StatusPixel.setPixelColor(0, 255, 0, 0, 0);
	StatusPixel.show();
	// digitalWrite(LEDPIN, HIGH); // update LED
	delay(delay1);
	StatusPixel.setPixelColor(0, 0, 0, 0, 0);
	StatusPixel.show();
	// digitalWrite(LEDPIN, LOW); // update LED
	delay(delay1);
	StatusPixel.setPixelColor(0, 255, 0, 0, 0);
	StatusPixel.show();
	// digitalWrite(LEDPIN, HIGH); // update LED
	delay(delay1);
	StatusPixel.setPixelColor(0, 0, 0, 0, 0);
	StatusPixel.show();
	// digitalWrite(LEDPIN, LOW); // update LED
	delay(delay1);
}
#pragma endregion
///////////////////////////////////////////////////////////////////////////////////////////////////

