#include <Adafruit_GFX.h>
#include <gfxfont.h>

// This file is the Entry Point for all things arduino firmware

//#include <Wire.h>

#include <Adafruit_NeoMatrix.h>
#include <gamma.h>

#include <Adafruit_NeoPixel.h>

#include <Adafruit_MotorShield.h>

// #include <Ultrasonic.h>

#include <Adafruit_BMP085_U.h>

#include <Adafruit_Sensor.h>

#include <Adafruit_LSM303_U.h>
#include <Adafruit_LSM303.h>

#define MotorShieldAddress 0x60

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

// #include </Adafruit_LSM303DLHC/Adafruit_LSM303_U.h>
// #include Adafruit_BMP085_U
#include <Adafruit_L3GD20_U.h>
// #include Adafruit_MotorShield
// #include <NewPing.h>
// #include Adafruit_NeoPixel
// #include Wire
// #include Adafruit_Sensor

#pragma region Global / Hard Coded Stuff

// long MotorTimeout; // this is the # of seconds before motors stop if no command has been received
// long MotorTimeoutPrevSec;



/* Assign a unique ID to this sensor at the same time */
Adafruit_LSM303_Mag_Unified mag = Adafruit_LSM303_Mag_Unified(1);
Adafruit_L3GD20_Unified gyro = Adafruit_L3GD20_Unified(2);
Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(3);
Adafruit_BMP085_Unified bmp = Adafruit_BMP085_Unified(4);

#define TrigC 11 //10 //11
#define EchoC 10 //11 //10
#define MaxDist 400
#define TrigR 13                       //8 //13
#define EchoR 12                       //9 //12
#define TrigL 9                        //12 //9
#define EchoL 8                        //13 //8
// NewPing sonarC(TrigC, EchoC, MaxDist); // ID: 1
// NewPing sonarR(TrigR, EchoR, MaxDist); // ID: 2
// NewPing sonarL(TrigL, EchoL, MaxDist); // ID: 3
// int SonarWait = 20;                    // min delay between each sonar pings
//long SonarPrevPing = 0;
//long SonarThisPing = 0;

//bool ConnectFlag;
//bool listenState; // are we in listening mode?
////Global Packet Parameters
byte rawPacket[20]; // setting up raw packet var

long prevMillis = 0; // used in loop for elapsed time
long prevSec = 0;

long LoopWait = 30.00; //arduinoParms.WaitInterval;
long waitLong = 250;

// const int ledPin = 13; // the number of the LED pin



// int ledState = LOW;

// int LEDindicator = HIGH;
// int LEDPin = 13;
// int LEDBrightness = 5;

//status pixel colors
int Red[7] = {0, 0, 0, 0, 0, 0, 0};
int Blue[7] = {0, 0, 0, 0, 0, 0, 0};
int Green[7] = {0, 0, 0, 0, 0, 0, 0};
int White[7] = {0, 0, 0, 0, 0, 0, 0};
#pragma endregion Global / Hard Coded Stuff

///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma region Enumerations
// arduino parameters that can be modified by rpi
enum ArduinoParms // unused
{
    waitInterval,  // the interval in ms between the next serial read
    brightnessLED, // the level of brightness the LEDs light up at (0 - 255)
    serialTimeout  // unused
};

// the header of the message that is usually used to determine what the rest of is for and
//how long the rest of the message will be
enum MessageHeader
{
    error,      // = 0 //= 343.343
    warning,    // = 1 //= 2401.2401
    information // = 2 //117.117
};

enum MessagesInformation
{
    unknownHeader,
    unknownCommand
};
//Error Message:
// 343.343
// error's location of origin

#pragma endregion Enumerations

//diagnosis loop - DO NOT REMOVE
//void loop()
//{
//  byte diag[] = { 58, 2, 6 };
//  GetSensorData(6);
//  DigestPacket(diag);
//  //SendSensorDataToRPi(6);
//}

void Motor_Init();
void Sensor_Init();
void Comms_Init();
void LED_Init();

//will need to set up a system to import changed parms from rpi
void setup()
{
    Serial.begin(9600); // start serial for output
    Serial.println("Serial began");
    Wire.begin();

    // //set up status LEDs
    LED_Init();
    // pinMode(LEDPIN, OUTPUT);
    // digitalWrite(LEDPIN, LOW); // update LED
    // StatusPixel.begin();
    // StatusPixel.show();
    // StatusPixel.setBrightness(LEDBrightness);

    AFMS.begin(); // create with the default frequency 1.6KHz
    ////AFMS.begin(1000);  // OR with a different frequency, say 1KHz

    // mag.enableAutoRange(true);
    // accel.enableAutoRange(true);
    // gyro.enableAutoRange(true);
    // bmp.enableAutoRange(true);

    // setNewColors(0, 0, 0, 255, 0, 1); // serial inactive (blue)

    // /* Initialize the sensor */
    // if (!mag.begin() || !accel.begin() || !gyro.begin() || !bmp.begin())
    // {
    //     /* There was a problem detecting the LSM303 ... check your connections */
    //     Serial.println("Ooops, a sensor was not detected ... Check your wiring!");
    //     WarningBlink();
    // }

    BootBlink();
    Motor_Init();
    // Sensor_Init();
    Comms_Init();
}
void Motor_SysTick();
void Sensor_SysTick();
void Comms_SysTick();
void UpdateLED();
// long MotorTimeoutPrevSec;
// long MotorTimeout;
//loop forever and read from rpi serial
void loop()
{
    unsigned long currentMillis = millis();

    // //only run loop after a certain amount of LoopWait time.
    if (currentMillis - prevMillis > LoopWait)
    {
        prevMillis = currentMillis;
        UpdateLED();
        // Sensor_SysTick();
    }

    //     setNewColors(0, 0, 0, 255, 0, 1);

    //     // if (Serial.available() > 0)
    //     // {
    //     //     setNewColors(0, 0, 255, 0, 0, 1); // set LED colors to "Serial active" color (green)
    //     //                                       //read bytes into array
    //     //     Serial.readBytesUntil(117, rawPacket, 30);
    //     //     RxPacket = rawPacket;
    //     //     //			DigestPacket(rawPacket);
    //     // }
    //     // // check if motor timeout period has elapsed if we do not receive any messages
    //     // else
    //     // {
    //     //     // if (currentMillis - MotorTimeoutPrevSec > MotorTimeout)
    //     //     // {
    //     //     // 	MotorTimeoutPrevSec = currentMillis;
    //     //     // 	// Motion motion = Motion();
    //     //     // 	// motion.StopRoverOnTimeout();
    //     //     // 	setNewColors(5, 255, 0, 0, 0, 0);
    //     //     // }
    //     // }
    // }
    // UpdateLED();
    
    Comms_SysTick();
    Motor_SysTick();
}

// END FILE

