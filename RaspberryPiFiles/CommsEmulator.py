
import RaspberryPiFiles.Packet as PacketFile
import RaspberryPiFiles.Motion as MotionFile
import time
Packet = PacketFile.Packet
ParameterPacket = PacketFile.ParameterPacket
Consume = PacketFile.Consume
Motion = MotionFile.Motion()


class SensorRequestPacket(Packet):
    def __init__(self, sensor):
        super().__init__(2)
        self.command1 = sensor
        self.CompilePacket()

while True:
    Motion.StopRover()

    time.sleep(2)

    Motion.MoveRoverForward(255)

    time.sleep(2)

    Motion.StopRover()

    time.sleep(2)

    Motion.StrafeRoverLeft(255)

    time.sleep(2)

    thisPacket = SensorRequestPacket(7)
    thisPacket.SendPacket()
    thisconsume = Consume.ConsumePacket()
    print(thisconsume)
