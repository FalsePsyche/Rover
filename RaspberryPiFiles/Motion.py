# all motion things go here
import RaspberryPiFiles.Packet as PacketFile
import multiprocessing as mp
import time

Packet = PacketFile.Packet


class MotorPacket(Packet):
    def __init__(self, m1_direction, m1_speed, m2_direction, m2_speed,
                 m3_direction, m3_speed, m4_direction, m4_speed):
        """

        Parameters
        ----------
        m1_speed: int
            0 - 100%
        m2_speed: int
            0 - 100%
        m1_direction: int
            0 = stop, 1 = forward, 2 = backward, 3 = unchanged (do not change motor direction or speed)
        m2_direction: int
            0 = stop, 1 = forward, 2 = backward, 3 = unchanged (do not change motor direction or speed)
        m3_speed: int
            0 - 100%
        m4_speed: int
            0 - 100%
        m3_direction: int
            0 = stop, 1 = forward, 2 = backward, 3 = unchanged (do not change motor direction or speed)
        m4_direction: int
            0 = stop, 1 = forward, 2 = backward, 3 = unchanged (do not change motor direction or speed)
        """
        super().__init__(1)
        self.command1 = m1_direction
        self.command2 = m1_speed
        self.command3 = m2_direction
        self.command4 = m2_speed
        self.command5 = m3_direction
        self.command6 = m3_speed
        self.command7 = m4_direction
        self.command8 = m4_speed
        self.CompilePacket()

    def CompilePacket(self):
        self.message.append(self.header)
        self.message.append(self.destination)
        self.message.append(self.command1)
        self.message.append(self.command2)
        self.message.append(self.command3)
        self.message.append(self.command4)
        self.message.append(self.command5)
        self.message.append(self.command6)
        self.message.append(self.command7)
        self.message.append(self.command8)
        self.message.append(117)  # need this because arduino relies on it to stop reading the serial


class Motion:
    # def StopRover(self, duration=0, sendNow=True):
    #     """
    #     Completely stops the rover by sending a stop command to both motors
    #
    #     Parameters
    #     ----------
    #     sendNow: bool
    #     duration: int
    #         number of seconds to sleep thread before moving on
    #
    #     Returns
    #     -------
    #     MotorPacket
    #         Returns the sent motor packet
    #     """
    #     # create thread so that we can have a duration before next command is sent
    #     stopThread = threading.Thread(target=self._StopRover(sendNow))
    #     stopThread.start()
    #
    # def _StopRover(self, sendNow):
    #     print("Stopping Rover")
    #     thisPacket = MotorPacket(4, 0)
    #     if sendNow is True:
    #         thisPacket.SendPacket()
    #     else:
    #         print("Rover Stop Failed.")

    # direction: 1 = forward, 0 = backward 2 = left, 3 = right, 4 = stop

    # region [Specific Motion Commands]

    def StopRover(self):
        """Stops all motors and stops the rover's movement"""
        self.MoveRover(0, 0, 0, 0, 0, 0, 0, 0)

    def TurnRoverRight(self, speed):
        """Turns all wheels clockwise to rotate the rover so that it turns counter clockwise(birds eye view)."""
        self.MoveRover(1, speed, 1, speed, 1, speed, 1, speed)

    def TurnRoverLeft(self, speed):
        """Turns all wheels counter clockwise to rotate the rover so that it turns clockwise(birds eye view)."""
        self.MoveRover(2, speed, 2, speed, 2, speed, 2, speed)

    def StrafeRoverLeft(self, speed):
        """Stops left and right motors and turns front and rear to strafe the rover to the left."""
        self.MoveRover(1, speed, 3, speed, 3, speed, 2, speed)

    def StrafeRoverRight(self, speed):
        """Stops left and right motors and turns front and rear to strafe the rover to the right."""
        self.MoveRover(2, speed, 3, speed, 3, speed, 1, speed)

    def MoveRoverForward(self, speed):
        """Stops front and rear motors and turns left and right to move rover forward.
        Parameters:
            speed 0 - 255
            """
        self.MoveRover(0, speed, 1, speed, 2, speed, 0, speed)

    def MoveRoverBackward(self, speed):
        """Stops front and rear motors and turns left and right to move rover backward."""
        self.MoveRover(0, speed, 2, speed, 1, speed, 0, speed)

    # endregion [Specific Motion Commands]

    def MoveRover(self, m1_direction=3, m1_speed=0, m2_direction=3, m2_speed=0,
                  m3_direction=3, m3_speed=0, m4_direction=3, m4_speed=0, sendNow=True):
        """
        Moves rover, or stops rover.
        Parameters
        ----------
        m1_speed: int
            0 - 100%
        m2_speed: int
            0 - 100%
        m1_direction: int
            0 = stop, 1 = forward, 2 = backward, 3 = unchanged (do not change motor direction or speed)
        m2_direction: int
            0 = stop, 1 = forward, 2 = backward, 3 = unchanged (do not change motor direction or speed)
        m3_speed: int
            0 - 100%
        m4_speed: int
            0 - 100%
        m3_direction: int
            0 = stop, 1 = forward, 2 = backward, 3 = unchanged (do not change motor direction or speed)
        m4_direction: int
            0 = stop, 1 = forward, 2 = backward, 3 = unchanged (do not change motor direction or speed)
        sendNow: bool
            should the packet be sent now. default is True

        Returns
        -------
        MotorPacket
            Returns the sent motor packet
        """
        # create thread so that we can have a duration before next command is sent
        # (not completely implemented as of 07 Jul 2016 10:26 AM)
        # moveThread = mp.Process(name="move",
        #                         target=self._MoveRover(speed=speed, direction=direction,
        #                                                duration=duration, sendNow=sendNow))
        # moveThread.start()
        self._MoveRover(m1_direction, m1_speed, m2_direction, m2_speed,
                        m3_direction, m3_speed, m4_direction, m4_speed, sendNow)

    def _MoveRover(self, m1_direction=3, m1_speed=0, m2_direction=3, m2_speed=0,
                   m3_direction=3, m3_speed=0, m4_direction=3, m4_speed=0, sendNow=True):
        print(
            "Moving Rover\n"
            "     Motor 1 | Speed: {0} | Direction: {1}\n"
            "     Motor 2 | Speed: {2} | Direction: {3}\n"
            "     Motor 3 | Speed: {4} | Direction: {5}\n"
            "     Motor 4 | Speed: {6} | Direction: {7}".format(
                str(m1_speed), str(m1_direction),
                str(m2_speed), str(m2_direction),
                str(m3_speed), str(m3_direction),
                str(m4_speed), str(m4_direction)))

        thisPacket = MotorPacket(m1_direction, m1_speed, m2_direction, m2_speed,
                                 m3_direction, m3_speed, m4_direction, m4_speed)
        if sendNow is True:
            thisPacket.SendPacket()
            # time.sleep(duration)  # sleep thread for the time being
