import RPi.GPIO as GPIO
import time
import statistics

GPIO.setmode(GPIO.BCM)

# Sensor Center
TrigC = 27
EchoC = 19

# Sensor Right
TrigR = 22
EchoR = 26

# Sensor L
TrigL = 17
EchoL = 13

print("distance measurement in progress")

GPIO.setup(TrigC, GPIO.OUT)
GPIO.setup(EchoC, GPIO.IN)

GPIO.setup(TrigR, GPIO.OUT)
GPIO.setup(EchoR, GPIO.IN)

GPIO.setup(TrigL, GPIO.OUT)
GPIO.setup(EchoL, GPIO.IN)

GPIO.output(TrigC, False)
GPIO.output(TrigR, False)
GPIO.output(TrigL, False)

print("waiting for sensors to settle")

time.sleep(2)

ArrayC = []
ArrayR = []
ArrayL = []

for count in range(0, 20):
    # Sensor C
    GPIO.output(TrigC, True)
    time.sleep(0.00001)
    GPIO.output(TrigC, False)

    while GPIO.input(EchoC) == 0:
        pulse_start = time.time()

    while GPIO.input(EchoC) == 1:
        pulse_end = time.time()

    # Sensor R
    GPIO.output(TrigR, True)
    time.sleep(0.00001)
    GPIO.output(TrigR, False)

    while GPIO.input(EchoR) == 0:
        pulse_startR = time.time()

    while GPIO.input(EchoR) == 1:
        pulse_endR = time.time()

    # Sensor L
    GPIO.output(TrigL, True)
    time.sleep(0.00001)
    GPIO.output(TrigL, False)

    while GPIO.input(EchoL) == 0:
        pulse_startL = time.time()

    while GPIO.input(EchoL) == 1:
        pulse_endL = time.time()

    pulse_durationC = pulse_end - pulse_start
    pulse_durationR = pulse_endR - pulse_startR
    pulse_durationL = pulse_endL - pulse_startL

    distanceC = pulse_durationC * 17150
    distanceR = pulse_durationR * 17150
    distanceL = pulse_durationL * 17150

    distanceC = round(distanceC, 2)
    distanceR = round(distanceR, 2)
    distanceL = round(distanceL, 2)

    print("Count: ", count)
    print("Distance C: ", distanceC, "cm")
    print("Distance R: ", distanceR, "cm")
    print("Distance L: ", distanceL, "cm")

    ArrayC.append(distanceC)
    ArrayR.append(distanceR)
    ArrayL.append(distanceL)

    time.sleep(0.2)

GPIO.cleanup()

print("Averages: ")
print("Center Sensor: ", round(statistics.mean(ArrayC), 2), "cm")
print("Right Sensor: ", round(statistics.mean(ArrayR), 2), "cm")
print("Left Sensor: ", round(statistics.mean(ArrayL), 2), "cm")
print("Standard Deviation: ")
print("Center Sensor: ", round(statistics.stdev(ArrayC), 2), "cm")
print("Right Sensor: ", round(statistics.stdev(ArrayR), 2), "cm")
print("Left Sensor: ", round(statistics.stdev(ArrayL), 2), "cm")
print("Variance: ")
print("Center Sensor: ", round(statistics.variance(ArrayC), 2), "cm")
print("Right Sensor: ", round(statistics.variance(ArrayR), 2), "cm")
print("Left Sensor: ", round(statistics.variance(ArrayL), 2), "cm")
if statistics.variance(ArrayC) > 1:
    print("Center Sensor has abnormally high variance in its data set!")
