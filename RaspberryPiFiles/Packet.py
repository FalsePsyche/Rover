
import serial
import time
from struct import *  # need this for unpacking packets of data from arduino
import multiprocessing as mutli

# device = '/dev/ttyACM0'
device = 'COM16'
baud = 9600
ser = serial.Serial(device, baud, timeout=3)


class Packet:
    header = 0
    destination = 0
    command1 = 0
    command2 = 0
    message = {}  # the message that will go out through serial

    def __init__(self, _dest):
        self.header = 58  # 58 is needed for arduino to consider the packet valid
        self.destination = _dest
        self.message = bytearray()

    def CompilePacket(self):
        self.message.append(self.header)
        self.message.append(self.destination)
        self.message.append(self.command1)
        self.message.append(self.command2)
        self.message.append(117)  # need this because arduino relies on it to stop reading the serial

    def SendPacket(self):
        # messageTotal = self.message.count(0)
        msg = "Sending: "
        for item in self.message:
            msg += "{0:03d}, ".format(item)

        print(msg)
        # ser.flush()
        ser.write(self.message)
        time.sleep(0.1)  # todo should use multiprocessing in the future


# region Packet Types
class ParameterPacket(Packet):
    """These packets are used to talk to arduino about parm values and such, that live on arduino.

    Targets:
        0 - Request handshake with arduino
        1 - LED control
            More information below.
        2 - Wait interval
            Values: 0 - 255 (recommended: 50 or above) (on boot default: 60)
        3 - Sonar wait interval
            Values: 0 - 255 (recommended: 50? or above) (on boot default: 50)
        4 - Motor Min Speed
            Values: 0 - 255 (on boot default: 100) (recommended: 50 is currently the lowest speed at which the
            rover still moves)
        5 - Motor Max Speed
            Values: 0 - 255 (on boot default: 255)
        6 - Motor Timeout
            Does not work right now


        LED Control:
            LED Targets:
                0 - brightness
                    Values: 0 - 255 (recommended: 5) (on boot default: 5)
                1 - LED color

    Examples:
        # to update the colors of LED 3 to very red with half white:
        update_LED_Colors = ParameterPacket(1, 1, 3, 255, 0, 0, 127)
        # to update the brightness (of all LEDs) to 10:
        update_LED_Brightness = ParameterPacket(1, 10)


    """
    def __init__(self, command1, command2, command3=-1, command4=-1, command5=-1, command6=-1):
        super().__init__(0)
        self.command1 = command1
        self.command2 = command2  # LoopWait, SonarDelay or brightness value or LED ID
        self.command3 = command3  # red value
        self.command4 = command4  # green
        self.command5 = command5  # blue
        self.command6 = command6  # white
        self.CompilePacket()

    def CompilePacket(self):
        self.message.append(self.header)
        self.message.append(self.destination)
        self.message.append(self.command1)
        self.message.append(self.command2)
        if self.command3 and self.command4 and self.command5 is not -1:  # ignore them if they are default values
            self.message.append(self.command3)  # red
            self.message.append(self.command4)  # green
            self.message.append(self.command5)  # blue
            self.message.append(self.command6)  # white
        self.message.append(117)  # need this because arduino relies on it to stop reading the serial


class MotionPacket(Packet):
    """These packets are used to send arduino motion commands"""
    def __init__(self, direction, speed):
        super().__init__(1)
        self.command1 = direction
        self.command2 = speed
        self.CompilePacket()


class SensorPacket(Packet):
    """These packets are used to request sensor data from arduino"""
    def __init__(self, sensor):
        super().__init__(2)
        self.command1 = sensor
        self.command2 = 0  # arduino doesn't use this right now
        self.CompilePacket()
        self.SendPacket()
# endregion Packet Types


# region Data Containers
class CompassData:
    """The architecture of a compass packet"""
    def __init__(self, X=0, Y=0, Z=0):
        self.header = 10
        self.X = X
        self.Y = Y
        self.Z = Z
        # todo this could take the data and copy it to a csv file to record it


class GyroData:
    def __init__(self, X=0, Y=0, Z=0):
        self.header = 20
        self.X = X
        self.Y = Y
        self.Z = Z


class AccelData:
    def __init__(self, X=0, Y=0, Z=0):
        self.header = 30
        self.X = X
        self.Y = Y
        self.Z = Z


class TPData:
    def __init__(self, temp=0, pressure=0):
        self.header = 40
        self.temp = temp
        self.pressure = pressure


class TenDOFData:
    def __init__(self, comppacket, gyroPacket, accelPacket, tpPacket):
        """

        Parameters
        ----------
        comppacket : CompassPacket object
        """
        self.header = 50
        self.compPacket = comppacket
        self.gyroPacket = gyroPacket
        self.accelPacket = accelPacket
        self.tpPacket = tpPacket


class SonarData:
    def __init__(self, centerSonar, leftSonar, rightSonar):
        self.header = 60
        self.centerSonar = centerSonar
        self.leftSonar = leftSonar
        self.rightSonar = rightSonar
# endregion Data Containers


# connect to the arduino; wait until it responds to a request
# this works really well if the arduino has a 10 uF cap from the GND to the Reset pin!
def Connect():
    print("Connecting to Arduino...")
    startTime = time.time()
    # set up interval vars
    prevSec1 = 0
    prevSec2 = time.time()
    checkInterval = 0.01
    resendInterval = 2
    connected = False
    # send initial request packet
    request = ParameterPacket(0, 0)
    request.SendPacket()
    # repeat connection check loop until valid response is received from arduino
    while connected is False:
        nowTime1 = time.time()
        if nowTime1 - prevSec1 > checkInterval:
            prevSec1 = time.time()  # update previous time
            # read in packet, if no packet then start the while loop over
            packet = ser.read(ser.inWaiting())
            if len(packet) >= 4:
                # thisPacket = ser.read(ser.inWaiting())
                handshake = unpack('f', packet[:4])
                print(str.format("Receiving: {0}", handshake[0]))
                check = int(handshake[0]) - 1970
                if -1 < check < 1:
                    print("arduino is ready")
                    connected = True
                    # request = ParameterPacket(1, 0)  # tell it to shut up
                    # request.SendPacket()
                # else:  # else no valid response
                #     connected = False
                #     request = ParameterPacket(0, 0)
                #     request.SendPacket()
        # resend request packet if no response after the resendInterval time amount has passed.
        if nowTime1 - prevSec2 > resendInterval:
            prevSec2 = time.time()
            # resend request packet
            request = ParameterPacket(0, 0)
            request.SendPacket()

    print("Please wait while we wait for the arduino to catch up...")
    ser.flush()
    # ser.read(ser.inWaiting())
    elapsed = time.time() - startTime
    print("Elapsed Time To Connect: " + str(elapsed))
    print("\n\n")


class Consume:

    packetData = []

    @staticmethod
    def ConsumePacket():
        # thread = mutli.Process(target=Consume._ConsumePacket())
        # thread.start()
        diff = 0
        timeout = 10
        startTime = time.time()
        nowTime = time.time()
        endTime = startTime
        packetList = []  # this will hold each data point (float) of the packet
        foodToConsume = True  # assume their is still food to consume
        message = 0
        while message is not 64 and diff < timeout:
            message = ser.inWaiting()
            # print("Size of packet waiting: " + str(message))
            nowTime = time.time()
            diff = nowTime - startTime
            time.sleep(0.01)
        if diff >= timeout:
            print("!!!!!!!!!!!!!!!!! Communication Timeout!")
        endTime = time.time()
        diff = endTime - startTime
        print("Number of seconds arduino took to send data: " + str(diff))

        if message >= 64:
            # packetData = []
            OGpacket = ser.read(ser.inWaiting())  # make an original packet var, do not want to modify this
            # OGpacket2 = ser.read_until(200)
            print("Packet Consumed: " + str(OGpacket))
            tempPacket = OGpacket
            print("Validating packet size: " + str(len(tempPacket)))
            checkHeader = unpack('f', tempPacket[:4])
            print("checking header: " + str(checkHeader[0]))
            # make sure we have a header AND the packet is large enough
            if 69 < checkHeader[0] < 71:
                print("valid header found")
                packetList.append(checkHeader[0])
                tempPacket = tempPacket[4:]  # remove the header
                # now get the rest of it
                thisPacket = unpack("fffffffffffffff", tempPacket)
                for item in thisPacket:
                    packetList.append(item)
                # while 200.100 < thisPacket < 200.300:  # EOL is 200.200
                #     thisPacket = unpack('f', tempPacket[:4])
                #     packetList.append(thisPacket[0])
                #     print(str(len(packetList)) + ". Appending: " + str(thisPacket[0]))
                #     tempPacket = tempPacket[4:]  # remove the captured data
                foodToConsume = False
                # after while loop; we have captured a complete set of sensor data
            else:
                tempPacket = tempPacket[4:]  # remove what we add to the list and then keep checking
        return packetList  # return the data!

    @staticmethod
    def _ConsumePacket():
        packetList = []  # this will hold each data point (float) of the packet
        foodToConsume = True  # assume their is still food to consume
        message = ser.inWaiting()
        if message >= 64:
            # packetData = []
            OGpacket = ser.read(ser.inWaiting())  # make an original packet var, do not want to modify this
            print("Packet Consumed: " + str(OGpacket))
            tempPacket = OGpacket
            print("Validating packet size: " + str(len(tempPacket)))
            while len(tempPacket) >= 4:
                checkHeader = unpack('f', tempPacket[:4])
                print("checking header: " + str(checkHeader[0]))
                # make sure we have a header AND the packet is large enough
                if 69 < checkHeader[0] < 71 and len(tempPacket) >= 60:
                    print("valid header found")
                    packetList.append(checkHeader[0])
                    tempPacket = tempPacket[4:]  # remove the header
                    # now get the rest of it
                    thisPacket = 10.10
                    while 200.100 < thisPacket < 200.300:  # EOL is 200.200
                        thisPacket = unpack('f', tempPacket[:4])
                        packetList.append(thisPacket[0])
                        print(str(len(packetList)) + ". Appending: " + str(thisPacket[0]))
                        tempPacket = tempPacket[4:]  # remove the captured data
                    foodToConsume = False
                    # after while loop; we have captured a complete set of sensor data
                else:
                    tempPacket = tempPacket[4:]  # remove what we add to the list and then keep checking
        # return packetList


        # while foodToConsume:
        #     # packetData = []
        #     OGpacket = ser.read(ser.inWaiting())  # make an original packet var, do not want to modify this
        #     print("Packet Consumed: " + str(OGpacket))
        #     tempPacket = OGpacket
        #     print("Validating packet size: " + str(len(tempPacket)))
        #     while len(tempPacket) >= 4:
        #         checkHeader = unpack('f', tempPacket[:4])
        #         print("checking header: " + str(checkHeader[0]))
        #         # make sure we have a header AND the packet is large enough
        #         if 69 < checkHeader[0] < 71 and len(tempPacket) >= 60:
        #             print("valid header found")
        #             packetList.append(checkHeader[0])
        #             tempPacket = tempPacket[4:]  # remove the header
        #             # now get the rest of it
        #             thisPacket = 10.10
        #             while 200.100 < thisPacket < 200.300:  # EOL is 200.200
        #                 thisPacket = unpack('f', tempPacket[:4])
        #                 packetList.append(thisPacket[0])
        #                 print(str(len(packetList)) + ". Appending: " + str(thisPacket[0]))
        #                 tempPacket = tempPacket[4:]  # remove the captured data
        #             foodToConsume = False
        #             # after while loop; we have captured a complete set of sensor data
        #         else:
        #             tempPacket = tempPacket[4:]  # remove what we add to the list and then keep checking
        # return packetList

            # if len(OGpacket) >= 4:
            #     packetheader = unpack('f', OGpacket[:4])  # get packet header
            #     packetheader = int(packetheader[0])  # convert to float
            #     print(str.format("Header: {0}", packetheader))
            #
            #     if packetheader is 10:  # compass
            #         packetData = unpack('ffffff', OGpacket[4:28])
            #         print(str.format("Packet Data: {0}", packetData))
            #         data = CompassData(packetData[0], packetData[1], packetData[2])
            #         return data
            #     elif packetheader is 20:  # gyro
            #         packetData = unpack('ffffff', OGpacket[4:28])
            #         print(str.format("Packet Data: {0}", packetData))
            #         data = GyroData(packetData[0], packetData[1], packetData[2])
            #         return data
            #     elif packetheader is 30:  # accel
            #         packetData = unpack('ffffff', OGpacket[4:28])
            #         print(str.format("Packet Data: {0}", packetData))
            #         data = AccelData(packetData[0], packetData[1], packetData[2])
            #         return data
            #     elif packetheader is 40:  # temp and pressure
            #         packetData = unpack('ff', OGpacket[4:12])
            #         print(str.format("Packet Data: {0}", packetData))
            #         data = TPData(packetData[0], packetData[1])
            #         return data
            #     elif packetheader is 50:
            #         print("place holder - tenDOF packet received")
            #         placeholder = 1  # do tenDOF here
            #     elif packetheader is 60:  # sonar
            #         packetData = unpack('fff', OGpacket[4:16])
            #         print(str.format("Packet Data: {0}", packetData))
            #         data = SonarData(packetData[0], packetData[1], packetData[2])
            #         return data
            #     elif packetheader is 70:
            #         print("place holder - all data packet received")
            #         placeholder = 1
            #     else:
            #         print("!!!!!!!!!! unidentified packet: " + str(OGpacket))
            #         placeholder = 1
            #     # todo remove extra stuff in packetData that is in packet
            #     # (right now this just cuts off the rest of whats in the packet)
            #     # packet = packet[len(packetData):]
            #     return packetData
            # else:
            #     print("Packet size smaller than 4!")
            #
            # if ser.inWaiting() > 0:
            #     print(str.format("items in serial waiting: {0}", ser.inWaiting()))
            #     foodToConsume = True
            # else:
            #     print("Nothing waiting!")
            #     foodToConsume = False


        # return packet in class form

    # def DigestPacket(self, packet):
    #     """
    #     Digest all packet types. This uses the header (byte[0]) to ID each packet type
    #
    #     Returns
    #     -------
    #
    #     """
    #     print("DIGESTING")
    #     foodLeft = True  # assume there is food left over
    #     packet = ser.read(1)
    #     while foodLeft:
    #         packetData = []
    #         packet += ser.read()
    #         # waiting = ser.inWaiting()
    #         if len(packet) > 0:
    #             print(str.format("items in serial waiting: {0}", len(packet)))
    #             foodLeft = True
    #         else:
    #             print("Nothing waiting!")
    #             foodLeft = False
    #
    #         print(str.format("Digest - Receiving: {0}", packet))
    #         if len(packet) >= 4:
    #             while foodLeft:
    #                 packetheader = unpack('i', packet)  # get packet header
    #                 print(str.format("Header: {0}", packetheader))
    #                 header = PacketEnums.Header
    #                 if packetheader is 10 or 20 or 30:  # compass, gyro, accel
    #                     # packet = ser.read(24)
    #                     packetData = unpack('ffffff', packet[1:])
    #                     print(str.format("Packet Data: {0}", packetData))
    #                     # return packetData
    #                 elif packetheader is 40:
    #                     # packet = ser.read(8)
    #                     packetData = unpack('ff', packet[1:])
    #                     # return packetData
    #                 elif packetheader is 50:
    #                     placeholder = 1  # do tenDOF here
    #                 elif packetheader is 60:  # sonar
    #                     # packet = ser.read(12)
    #                     packetData = unpack('fff', packet[1:])
    #                     # return packetData
    #                 elif packetheader is header.allType:
    #                     placeholder = 1
    #                 # remove stuff in packetData that is in packet
    #                 # (right now this just cuts off the rest of whats in the packet)
    #                 packet = packet[len(packetData):]
    #                 foodLeft = False
    #
    #                 # if len(packet) is 0:
    #                 #     foodLeft = False
    #                 # else:
    #                 #     foodLeft = True
    #             return packetData


