import serial
import time

ser = serial.Serial('/dev/ttyACM0', 9600)


def writeNumber(value):
    # bus.write_byte(address, value)
    ser.write(value.encode())


def readNumber():
    # number = bus.read_byte(address)
    _number = ser.read()
    return _number


var = 0
while 0 <= var < 1024:
    var = 0
    print("RPI: Hi Arduino, I sent you ", var)
    writeNumber(str(var))
    # sleep one second
    time.sleep(1)

    number = readNumber()
    print("Arduino: Hey RPI, I received a digit ", number)
    time.sleep(1)

    var = 1
    print("RPI: Hi Arduino, I sent you ", var)
    writeNumber(str(var))
    time.sleep(1)

    number = readNumber()
    print("Arduino: Hey RPI, I received a digit ", number)
    time.sleep(1)
