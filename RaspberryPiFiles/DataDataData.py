# this is where all the data base logic goes.

import sqlite3
import time

# filename = "test.sqlite"
tablename = "main"
index_column = "index column"
timestamp_column = "timestamp column"
compass_heading = "compass heading column"
sonar_center = "center sonar column"
sonar_right = "right sonar column"
sonar_left = "left sonar column"

# create database in memory
con = sqlite3.connect(":memory:")
con.isolation_level = None
cur = con.cursor()

buffer = ""

# Creating a new SQLite table with 1 column
# cur.execute('CREATE TABLE {tn}({nf} REAL)'.format(tn=tablename, nf=index_column))
cur.execute('CREATE TABLE main(indexcolumn REAL)')

# initialize database table
cur.execute("ALTER TABLE {tn} ADD COLUMN '{cn}' REAL".format(tn=tablename, cn=index_column))
cur.execute("ALTER TABLE {tn} ADD COLUMN '{cn}' REAL".format(tn=tablename, cn=timestamp_column))
cur.execute("ALTER TABLE {tn} ADD COLUMN '{cn}' REAL".format(tn=tablename, cn=compass_heading))
cur.execute("ALTER TABLE {tn} ADD COLUMN '{cn}' REAL".format(tn=tablename, cn=sonar_center))
cur.execute("ALTER TABLE {tn} ADD COLUMN '{cn}' REAL".format(tn=tablename, cn=sonar_right))
cur.execute("ALTER TABLE {tn} ADD COLUMN '{cn}' REAL".format(tn=tablename, cn=sonar_left))


# def insert_new(index, time, compass, sonarC, sonarR, sonarL):
index = 1
# Inserts an ID with a specific value in a second column
try:
    cur.execute("INSERT INTO {tn} ({idf}, {cn}) VALUES ({idx}, {time})".
                format(tn=tablename, idf=index_column, cn=timestamp_column, idx=index, time=time.time()))
except sqlite3.IntegrityError:
    print('ERROR: ID already exists in PRIMARY KEY column {}'.format(index_column))

con.commit()
con.close()
