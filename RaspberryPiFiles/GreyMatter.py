# for brains goal 1, this file will contain the higher level systems
# for example: turning compass data into a high level idea of which way the robot is facing

import math
import RaspberryPiFiles.Motion as MotionFile
import RaspberryPiFiles.Packet as PacketFile


Motion = MotionFile.Motion()
Consume = PacketFile.Consume()
# Packet = PacketFile.Packet(0)

# todo make isRoverUpRight: this will check the gyro to see if the rover is upright or not


class Sense:
    """
    All the rovers "senses"
    """
    class Magnetoception:
        """North, South, East or West. Pick your poison"""
        def getSense(self, x, y):
            """Returns a compass heading (degrees) as a float. This can be used to know which way the rover is facing
            at any given point in time."""
            # return heading from compass data
            # magnetic declination: 5.88333 degrees, 0.1026834795 radians
            # from http://www.magnetic-declination.com/
            declinationAngle = 0.103
            # // Hold the module so that Z is pointing 'up' and you can measure the heading with x&y
            # // Calculate heading when the magnetometer is level, then correct for signs of axis.
            arctangent = math.atan2(y, x)
            """
            // Once you have your heading, you must then add your 'Declination Angle', which is the 'Error' of the
            // magnetic field in your location.
            // Find yours here: http://www.magnetic-declination.com/
            // Mine is: -13* 2' W, which is ~13 Degrees, or (which we need) 0.22 radians
            // If you cannot find your Declination, comment out these two lines, your compass will be slightly off.
            """
            thisHeading = arctangent + declinationAngle
            # // Correct for when signs are reversed.
            if thisHeading < 0:
                thisHeading += 2 * math.pi
            # // Check for wrap due to addition of declination.
            if thisHeading > 2 * math.pi:
                thisHeading -= 2 * math.pi
            # // Convert radians to degrees for readability.
            heading = thisHeading * 180 / math.pi
            return heading

    class Proxemics:
        """Personal space is important."""
        # sonar sensors and stuff
        def roverIsToClose(self, sonarID=0, distance=10):
            """

            Parameters
            ----------
            sonarID
                Which sensor are we talking about here? Center, Right or Left?
            distance
                A radius of this amount of centimeters is where the personal space is defined.

            Returns
            -------

            """
            # if sonarID is 0:
                # get all sensor data
                # todo do all sensors need to be within the threshold? or just one. figure this out
            # if sonar value is less than distance var then personal space has been invaded

    class Equilibrioception:
        """Equilibrium in one's life is boring"""


class SmartRobot:
    """A smart robot does not what she's told, but what she tells herself to do.

    Here are the high level scripts for different "modes"; like exploration and stuff"""
    class DumbExploration:
        """rover will move forward until it gets within 10cm of an object (center sonar).
        and when this happens the rover will turn 45 degrees to the right."""
        def avoidObjectCollision(self, sonarCenter, oldCH, newCH):
            while self.roverIsCloseToObject(sonarCenter):
                Motion.MoveRover(75, 3)
                # Packet.
                # todo should have global vars of the most recent data packets from arduino? and have the
                # talking done on a separate thread?

        def roverIsCloseToObject(self, sonarCenter):
            # return true or false
            if sonarCenter < 10:
                return True
            else:
                return False

        def roverHasTurn45Degrees(self, oldCH, newCH):
            if oldCH - newCH > 45:
                return True
            else:
                return False
