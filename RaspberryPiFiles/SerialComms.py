# This file's goal is to create, send and receive communication information through USB
# serial to and from the arduino.

import math
import tabulate
import time
import sys
import csv
import statistics
import os
from enum import Enum
from datetime import datetime
import numpy as num
# import matplotlib.pyplot as plt
import RaspberryPiFiles.Packet as PacketFile
import RaspberryPiFiles.Motion as MotionFile
import RaspberryPiFiles.GreyMatter as GreyMatterFile

Packet = PacketFile.Packet
ParameterPacket = PacketFile.ParameterPacket
Consume = PacketFile.Consume
Motion = MotionFile.Motion()
GreyMatter = GreyMatterFile.Sense()


# todo create system to turn rover until compass data says the rover has turned 45 degrees


class MotorEnums(Enum):
    class Move:
        forward = 0
        backward = 1
        left = 2
        right = 3
        stop = 4

    class Speed:
        """
            In percentages. Stop = 0
        """
        stop = 0
        slow = 25
        normal = 50
        fast = 75
        ludicrous = 100


class PacketEnums(Enum):
    class Header:
        compass = 10
        gyro = 20
        accel = 30
        temppress = 40
        tenDOF = 50
        sonar = 60
        allType = 70


class SensorRequestPacket(Packet):
    def __init__(self, sensor):
        super().__init__(2)
        self.command1 = sensor
        self.CompilePacket()
        # Sensor mapping:
        # 1 = compass
        # 2 = Gyro
        # 3 = Accel
        # 4 = temp and pressure
        # 5 = all 10DOF
        # 6 = all sonar
        # 7 = all of the above

        # Read Compass, Gyro or Accel packet
        # def ReadCGAPacket(self):
        #     waiting = ser.inWaiting()
        #     thisPacket = ser.read(waiting)
        #     length = len(thisPacket)
        #     print("ReadCGAPacket Packet Size: " + str(length))
        #     if len(thisPacket) is 8:
        #         this = unpack("ff", thisPacket)
        #     elif len(thisPacket) is 12:
        #         this = unpack("fff", thisPacket)
        #     elif len(thisPacket) is 16:
        #         this = unpack("ffff", thisPacket)
        #     elif len(thisPacket) is 20:
        #         this = unpack("fffff", thisPacket)
        #     elif len(thisPacket) is 24:
        #         this = unpack("ffffff", thisPacket)
        #     elif len(thisPacket) is 28:
        #         this = unpack("fffffff", thisPacket)
        #     elif len(thisPacket) is 32:
        #         this = unpack("ffffffff", thisPacket)
        #     elif len(thisPacket) is 36:
        #         this = unpack("fffffffff", thisPacket)
        #     elif len(thisPacket) is 40:
        #         this = unpack("ffffffffff", thisPacket)
        #     elif len(thisPacket) is 44:
        #         this = unpack("fffffffffff", thisPacket)
        #     elif len(thisPacket) is 48:
        #         this = unpack("ffffffffffff", thisPacket)
        #     elif len(thisPacket) is 52:
        #         this = unpack("fffffffffffff", thisPacket)
        #     elif len(thisPacket) is 56:
        #         this = unpack("ffffffffffffff", thisPacket)
        #     elif len(thisPacket) is 60:
        #         this = unpack("fffffffffffffff", thisPacket)
        #     elif len(thisPacket) is 64:
        #         this = unpack("ffffffffffffffff", thisPacket)
        #     # print(thisPacket)
        #     # if len(thisPacket) > 24:
        #     #     sensorData = unpack("ffffff", thisPacket[0:24])
        #     #     return sensorData
        #     if len(thisPacket) is 24:  # must be 24 length to be able to unpack
        #         # verify = unpack('f', thisPacket[0:4])
        #         # print(verify)
        #         # check = int(verify[0]) - 10.10
        #         # print(check)
        #         # if -1 < check < 1:
        #         # print("unpacking...")
        #         sensorData = unpack("ffffff", thisPacket)
        #         print(str(sensorData))
        #         # self.FormatCompassPacket(sensorData)
        #         return sensorData  # NOTE: compass data received is working
        #     elif len(thisPacket) > 3:
        #         this = unpack("f", thisPacket[:4])
        #         thing = 1
        #     else:
        #         print("packet data size no good: ", len(thisPacket))
        #
        # def ReadSonarPacket(self):
        #     thisPacket = ser.read(ser.inWaiting())
        #     # print(thisPacket)
        #     if len(thisPacket) is 24:
        #         sensorData = unpack("ffffff", thisPacket)
        #         print(str(sensorData))
        #         # self.FormatSonarPacket(thisPacket)
        #         return sensorData
        #     else:
        #         print("packet data size no good: ", len(thisPacket))
        #
        # # Read Temperature or Pressure packet
        # def ReadTPPacket(self):
        #     if ser.inWaiting() is 0:
        #         print("Nothing to read")
        #         return False
        #     else:
        #         thisPacket = ser.read(ser.inWaiting())
        #         sensorData = unpack("iff", thisPacket)
        #         return sensorData
        #
        # def DigestPacket(self):
        #     """
        #     Digest all packet types. This uses the header (byte[0]) to ID each packet type
        #
        #     Returns
        #     -------
        #
        #     """
        #     foodLeft = True  # assume there is food left over
        #     packet = ser.read(1)
        #     while foodLeft:
        #         packetData = []
        #         packet += ser.read()
        #         # waiting = ser.inWaiting()
        #         if len(packet) > 0:
        #             print(str.format("items in serial waiting: {0}", len(packet)))
        #             foodLeft = True
        #         else:
        #             print("Nothing waiting!")
        #             foodLeft = False
        #
        #         print(str.format("Digest - Receiving: {0}", packet))
        #         if len(packet) >= 4:
        #             while foodLeft:
        #                 packetheader = unpack('i', packet)  # get packet header
        #                 print(str.format("Header: {0}", packetheader))
        #                 header = PacketEnums.Header
        #                 if packetheader is 10 or 20 or 30:  # compass, gyro, accel
        #                     # packet = ser.read(24)
        #                     packetData = unpack('ffffff', packet[1:])
        #                     print(str.format("Packet Data: {0}", packetData))
        #                     # return packetData
        #                 elif packetheader is 40:
        #                     # packet = ser.read(8)
        #                     packetData = unpack('ff', packet[1:])
        #                     # return packetData
        #                 elif packetheader is 50:
        #                     placeholder = 1  # do tenDOF here
        #                 elif packetheader is 60:  # sonar
        #                     # packet = ser.read(12)
        #                     packetData = unpack('fff', packet[1:])
        #                     # return packetData
        #                 elif packetheader is header.allType:
        #                     placeholder = 1
        #                 # remove stuff in packetData that is in packet
        #                 # (right now this just cuts off the rest of whats in the packet)
        #                 packet = packet[len(packetData):]
        #                 foodLeft = False
        #
        #                 # if len(packet) is 0:
        #                 #     foodLeft = False
        #                 # else:
        #                 #     foodLeft = True
        #             return packetData


# checks if packet value is hand shake
# def IsHandShake(packet):
#     leftovers = True
#     while leftovers:
#         if len(packet) >= 4:
#
#             check = int(packet[0]) - 1970
#             if -1 < check < 1:
#                 # return packet without the handshake value
#                 return packet[4:]


# class XYZData:
#     def __init__(self):
#         # self = self
#         self.header = []  # this is the sensor data type
#         self.x = []
#         self.y = []
#         self.z = []
#         self.time = []
#
#     def __len__(self):
#         count = len(self.x)
#         return count
#
#     # todo not working
#     def append(self, packet):
#         # if len(packet) > 16:
#             self.header.append(packet[0])
#             self.x.append(packet[1])
#             self.y.append(packet[2])
#             self.z.append(packet[3])
#             self.time.append(time.time())

# DUMB EXPLORATION MODE STARTS HERE ############################################################################
# print("Giving human time to react...")
# time.sleep(5)

fastSpeed = 15
slowSpeed = 5


class DumbExploration:
    def __init__(self, direction=1):
        self.direction = direction  # one is left, 2 is right
        self.sideThreshold = 3  # 4 cm for side sonars (too close)
        self.centerThreshold = 20
        self.SC = 0  # Center Sonar reading
        self.SR = 0
        self.SL = 0
        # only set this when we create the object or to update it in roverHasTurn45Degrees
        self.newCH = 0
        self.dataTable = []
        self.timeStart = time.time()
        self.updateSensorData()
        self.oldCH = self.newCH

    def updateSensorData(self):
        Motion.StopRover()  # stop rover before getting data to see if this increases accuracy
        thisPacket = SensorRequestPacket(7)
        thisPacket.SendPacket()
        thisconsume = Consume.ConsumePacket()
        self.SC = thisconsume[12]
        self.SR = thisconsume[13]
        self.SL = thisconsume[14]
        self.newCH = self._getCompassHeading(thisconsume[1], thisconsume[2])
        self.dataTable.append([self.SC, self.SR, self.SL, self.newCH, time.time() - self.timeStart])

    def _getCompassHeading(self, thisX, thisY):
        # headings in degrees:
        # South = 180, North = 360/0, East = 90, West = 270
        declinationAngle = 0.103
        # calculate heading in degrees to get basic cardinal directions yo
        # // Hold the module so that Z is pointing 'up' and you can measure the heading with x&y
        # // Calculate heading when the magnetometer is level, then correct for signs of axis.
        arctangent = math.atan2(thisY, thisX)
        thisHeading = arctangent + declinationAngle
        # // Correct for when signs are reversed.
        if thisHeading < 0:
            thisHeading += 2 * math.pi
        # // Check for wrap due to addition of declination.
        if thisHeading > 2 * math.pi:
            thisHeading -= 2 * math.pi
        # // Convert radians to degrees for readability.
        heading = thisHeading * 180 / math.pi
        return heading

    def avoidObjectCollision(self):
        # is rover to close to an object?
        while self.roverIsCloseToObject():
            # has rover turned 45 degrees to avoid that object?
            while self.roverHasTurn45Degrees() is False:  # or self.SC > self.centerThreshold * 2:
                self.turnForOneSecond()

    def roverIsCloseToObject(self):
        self.updateSensorData()
        if self.SC < self.centerThreshold:
            print("a. Rover is too close: " + str(self.SC))
            Motion.StopRover()  # stop rover so we do not collide
            return True
        elif self.SR < self.sideThreshold:
            print("a. Rover is too close: " + str(self.SC))
            Motion.StopRover()  # stop rover so we do not collide
            return True
        elif self.SL < self.sideThreshold:
            print("a. Rover is too close: " + str(self.SC))
            Motion.StopRover()  # stop rover so we do not collide
            return True
        else:
            print("a. Rover is NOT too close: " + str(self.SC))
            return False

    def turnForOneSecond(self):
        if self.direction == 1:
            Motion.TurnRoverLeft(10)
        elif self.direction == 2:
            Motion.TurnRoverRight(10)
        # self.turnDirection()
        time.sleep(0.1)

    def roverHasTurn45Degrees(self):
        self.updateSensorData()
        print("\nb. old CH: " + str(self.oldCH))
        print("b. new CH: " + str(self.newCH))
        if math.fabs(self.oldCH - self.newCH) > 45:
            print("b. Completed 45 degree turn")
            self.oldCH = self.newCH  # update old CH if we have turned enough
            return True
        else:
            return False

    # this is having an issue where the rover will turn left then right and left again forever
    def turnDirection(self):
        # if wall is closer on the left side then turn right
        if self.SR > self.SL:
            Motion.TurnRoverLeft(fastSpeed)
        elif self.SR < self.SL:
            Motion.TurnRoverRight(fastSpeed)

    # def rotate(self):
    #     if self.direction == 1:
    #         self.direction = 2
    #     elif self.direction == 2:
    #         self.direction = 1
    #
    #     self.updateSensorData()
    #     print("///////////////////////////////////////////////////////////")
    #     print("South")
    #     south = self.newCH
    #     print("compass heading: " + str(south))
    #     while self.roverHasTurn45Degrees() is False:
    #         self.turnForOneSecond()
    #     print("///////////////////////////////////////////////////////////")
    #     print("SouthWest")
    #     print("compass heading: " + str(self.newCH))
    #     while self.roverHasTurn45Degrees() is False:
    #         self.turnForOneSecond()
    #     print("///////////////////////////////////////////////////////////")
    #     print("West")
    #     west = self.newCH
    #     print("compass heading: " + str(west))
    #     while self.roverHasTurn45Degrees() is False:
    #         self.turnForOneSecond()
    #     print("///////////////////////////////////////////////////////////")
    #     print("NorthWest")
    #     print("compass heading: " + str(self.newCH))
    #     while self.roverHasTurn45Degrees() is False:
    #         self.turnForOneSecond()
    #     print("///////////////////////////////////////////////////////////")
    #     print("North")
    #     north = self.newCH
    #     print("compass heading: " + str(north))
    #     while self.roverHasTurn45Degrees() is False:
    #         self.turnForOneSecond()
    #     print("///////////////////////////////////////////////////////////")
    #     print("NorthEast")
    #     print("compass heading: " + str(self.newCH))
    #     while self.roverHasTurn45Degrees() is False:
    #         self.turnForOneSecond()
    #     print("///////////////////////////////////////////////////////////")
    #     print("East")
    #     east = self.newCH
    #     print("compass heading: " + str(east))
    #     while self.roverHasTurn45Degrees() is False:
    #         self.turnForOneSecond()
    #     print("///////////////////////////////////////////////////////////")
    #     print("SouthEast")
    #     print("compass heading: " + str(self.newCH))
    #     while self.roverHasTurn45Degrees() is False:
    #         self.turnForOneSecond()
    #     print("///////////////////////////////////////////////////////////")
    #     print("end rotate")
    #     print("///////////////////////////////////////////////////////////")
    #     Motion.StopRover()
    #     print("south: " + str(south))
    #     print("west: " + str(west))
    #     print("sw diff: " + str(south - west))
    #     print("north: " + str(north))
    #     print("nw diff: " + str(north - west))
    #     print("east: " + str(east))
    #     print("ne diff: " + str(north - east))
    #     print("se diff: " + str(south - east))
    #
    #     table = []
    #     headers = ["Assumed Cardinal Direction", "Recorded Degrees"]
    #     table.append(["South", south])
    #     table.append(["East", east])
    #     table.append(["North", north])
    #     table.append(["West", west])
    #
    #     print("\n" + tabulate.tabulate(table, headers) + "\n")
    #
    #     table = []
    #     headers = ["Assumed Cardinal Directions", "Difference in Degrees"]
    #     table.append(["South - West", south - west])
    #     table.append(["West - North", west - north])
    #     table.append(["East - North", east - north + 360])
    #     table.append(["East - South", east - south])
    #
    #     print("\n" + tabulate.tabulate(table, headers) + "\n")
    #
    #     table = []
    #     headers = ["Sonar Center", "Sonar Right", "Sonar Left", "Heading", "Heading Diff", "Time Elapsed", "Time Diff"]
    #     prevTime = 0
    #     prevHeading = 0
    #     for line in self.dataTable:
    #         thisTime = line[4]
    #         thisHeading = line[3]
    #         diffTime = thisTime - prevTime
    #         line.append(diffTime)
    #         diffHeading = thisHeading - prevHeading
    #         line.insert(4, diffHeading)
    #         table.append(line)
    #         prevTime = thisTime
    #         prevHeading = thisHeading
    #
    #     print("\n" + tabulate.tabulate(table, headers) + "\n")
    #
    #     self.dataTable.clear()
    #     return table

    def rotate(self):
        if self.direction == 1:
            self.direction = 2
        elif self.direction == 2:
            self.direction = 1

        self.updateSensorData()
        thisCount = 0
        while thisCount < 9:  # do it 8 times (45 degree segments in 350 degrees)
            while self.roverHasTurn45Degrees() is False:
                self.turnForOneSecond()
            thisCount += 1

        Motion.StopRover()

        table = []
        headers = ["Sonar Center", "Sonar Right", "Sonar Left", "Heading", "Heading Diff", "Time Elapsed", "Time Diff"]
        prevTime = 0
        prevHeading = 0
        for line in self.dataTable:
            thisTime = line[4]
            thisHeading = line[3]
            diffTime = thisTime - prevTime
            line.append(diffTime)
            diffHeading = thisHeading - prevHeading
            line.insert(4, diffHeading)
            table.append(line)
            prevTime = thisTime
            prevHeading = thisHeading

        print("\n" + tabulate.tabulate(table, headers) + "\n")

        self.dataTable.clear()
        return table

PacketFile.Connect()
sonarDelay = 0
PacketFile.ParameterPacket(4, sonarDelay).SendPacket()  # start delay at zero
PacketFile.ParameterPacket(5, 50).SendPacket()  # tell arduino to set motor min speed to 50 instead of 100
# Motion.MoveRoverForward(10)
# time.sleep(1)
# Motion.MoveRoverBackward(10)
# time.sleep(1)
# Motion.TurnRoverLeft(10)
# time.sleep(5)
# Motion.TurnRoverRight(10)
# time.sleep(5)
# print("Direction: 1")
# print("Motor 1")
# Motion.MoveRover(1, 100, 0, 0, 0, 0, 0, 0)
# time.sleep(5)
# print("Motor 2")
# Motion.MoveRover(0, 0, 1, 100, 0, 0, 0, 0)
# time.sleep(5)
# print("Motor 3")
# Motion.MoveRover(0, 0, 0, 0, 1, 100, 0, 0)
# time.sleep(5)
# print("Motor 4")
# Motion.MoveRover(0, 0, 0, 0, 0, 0, 1, 100)
# time.sleep(5)
# print("Direction: 2")
# print("Motor 1")
# Motion.MoveRover(2, 100, 0, 0, 0, 0, 0, 0)
# time.sleep(5)
# print("Motor 2")
# Motion.MoveRover(0, 0, 2, 100, 0, 0, 0, 0)
# time.sleep(5)
# print("Motor 3")
# Motion.MoveRover(0, 0, 0, 0, 2, 100, 0, 0)
# time.sleep(5)
# print("Motor 4")
# Motion.MoveRover(0, 0, 0, 0, 0, 0, 2, 100)
# time.sleep(5)
# Motion.TurnRoverRight(25)
# time.sleep(3)
# Motion.StrafeRoverLeft(25)
# time.sleep(3)

print("Testing default full speed.")
Motion.StopRover()
Motion.TurnRoverRight(100)
time.sleep(1)
Motion.TurnRoverLeft(100)
time.sleep(1)
Motion.StopRover()

dumbMode = DumbExploration()
bigDataTable = []
headers1 = ["Sonar Center", "Sonar Right", "Sonar Left",
            "Heading", "Heading Diff",
            "Time Elapsed", "Time Diff", "Session", "Sonar Delay (ms)"]

outerCount = 0
count = 0
while outerCount < 40:
    while count < 4:  # * 24 = estimated time in seconds
        bigDataStorage = dumbMode.rotate()
        for thisline in bigDataStorage:
            session = count
            thisline.append(session)
            thisline.append(sonarDelay)
            bigDataTable.append(thisline)
        count += 1
        PacketFile.ParameterPacket(4, sonarDelay).SendPacket()  # start delay at zero
        bigDataStorage.clear()
    sonarDelay += 10
    count = 0  # reset inner count


print("\n" + tabulate.tabulate(bigDataTable, headers1) + "\n")

# a = []  # any heading from 0 to 90
# b = []  # any heading from 90.1 to 180
# c = []  # any heading from 180.1 to 270
# d = []  # any heading from 270.1 to 360
# e = []  # anything out of range
#
# for thisline in bigDataTable:
#     thisHeading = thisline[3]
#     thisSonarCenter = thisline[0]
#     thisSonarLeft = thisline[2]
#     thisSonarRight = thisline[1]
#     if thisHeading >= 90:
#         a.append([thisHeading, thisSonarCenter, thisSonarLeft, thisSonarRight])
#     elif 90 < thisHeading <= 180:
#         b.append([thisHeading, thisSonarCenter, thisSonarLeft, thisSonarRight])
#     elif 180 < thisHeading <= 270:
#         c.append([thisHeading, thisSonarCenter, thisSonarLeft, thisSonarRight])
#     elif 270 < thisHeading <= 360:
#         d.append([thisHeading, thisSonarCenter, thisSonarLeft, thisSonarRight])
#     else:
#         e.append([thisHeading, thisSonarCenter, thisSonarLeft, thisSonarRight])


# making the graph
xCenter = []
yCenter = []
xLeft = []
yLeft = []
xRight = []
yRight = []
allPoints = []  # left(x,y), center(x,y), right(x,y)
uberHeaders = ["Left X", "Left Y", "Center X", "Center Y", "Right X", "Right Y",
               "Left Distance (cm)", "Center Distance (cm)", "Right Distance (cm)",
               "Left Heading (degrees)", "Center Heading", "Right Heading",
               "Time Elapsed (sec)", "Time Diff (sec)", "Rotation Direction", "Sonar Delay (ms)"]
uberData = []

# do center
for thisline in bigDataTable:
    # get time data
    thisTime = thisline[5]
    thisTimeDiff = thisline[6]
    thisSession = thisline[7]
    thisSonarDelay = thisline[15]
    # do center
    thisAngleC = thisline[3]
    thisDistanceC = thisline[0]
    xC = thisDistanceC * num.sin(thisAngleC)
    yC = thisDistanceC * num.cos(thisAngleC)
    yCenter.append(xC)
    xCenter.append(yC)
    # do left
    thisDistanceL = thisline[2]
    thisAngleL = thisAngleC - 45
    if thisAngleL < 0:
        thisAngleL += 360
    xL = thisDistanceL * num.sin(thisAngleL)
    yL = thisDistanceL * num.cos(thisAngleL)
    yLeft.append(xL)
    xLeft.append(yL)
    # do right
    thisDistanceR = thisline[1]
    thisAngleR = thisAngleC + 45
    if thisAngleR > 360:
        thisAngleR -= 360
    xR = thisDistanceR * num.sin(thisAngleR)
    yR = thisDistanceR * num.cos(thisAngleR)
    yRight.append(xR)
    xRight.append(yR)

    allPoints.append([xL, yL, xC, yC, xR, yR])
    uberData.append([xL, yL, xC, yC, xR, yR,
                     thisDistanceL, thisDistanceC, thisDistanceR,
                     thisAngleL, thisAngleC, thisAngleR,
                     thisTime, thisTimeDiff, thisSession, thisSonarDelay])

print("total lines: " + str(len(uberData)))

# csv with plot points
filename = "PlotPointsDataSet"
hour = datetime.now().hour
minute = datetime.now().minute
filename = filename + "_" + str(hour) + "_" + str(minute)

with open(filename + ".csv", 'wt') as f:
    writer = csv.writer(f)
    writer.writerow(uberHeaders)
    for thisline in uberData:
        print(thisline)
        writer.writerow(thisline)

# print(open("dataSet.csv", 'rt').read())

# startTime = time.time()
# duration = 30  # seconds
# endTime = startTime + duration
# currentTime = startTime
# dumbMode = DumbExploration()
# # count1 = 0
# dumbMode.updateSensorData()
# print("compass heading: " + str(dumbMode.newCH))
# print("Center Sonar: " + str(dumbMode.SC))

# while currentTime < endTime:
#     # start moving rover forward
#     Motion.MoveRover(60)
#     dumbMode.avoidObjectCollision()
#     currentTime = time.time()
#     count1 += 1

Motion.StopRover()
stop = 1

print("End")

# print("///////////// Main while loop count: " + str(count1))
# DUMB EXPLORATION MODE ENDS HERE ############################################################################


# sensor testing
# # time.sleep(1)
# # Motion.StopRover()
# # time.sleep(1)
# # Motion.MoveRover(100, 2)
# # time.sleep(1)
# # Motion.MoveRover(100, 3)
# # time.sleep(1)
# # Motion.StopRover()
#

# compass = XYZData()
# sonar = XYZData()

# todo need to set up system to check first byte and use it to correctly identify the packet type
# todo also find limitations: eg how much data can we collect from arduino and how fast? do we need to collect and store
# todo... it on the arduino first? or can we collect it fast enough that the rpi can store it.
# todo data collection needs timestamps
#
#
# number = 10.32134624352
# string = "%.3f" % number
# print(string)
# PacketFile.Connect()

# region TEST ARDUINO PARM UPDATE
# LED control
# print("updating LED 5")
# ledColor = ParameterPacket(2, 5, 255, 0, 0, 255)
# ledColor.SendPacket()
# print("LED 5 should now be red and white")
# # LED brightness
# print("updating LED brightness to 255")
# ledBright = ParameterPacket(1, 150)
# ledBright.SendPacket()
# time.sleep(3)
# print("updating LED brightness to default (5)")
# ledBright = ParameterPacket(1, 5)
# ledBright.SendPacket()
# time.sleep(0.5)
# print("updating wait interval to 255ms")
# waitInterval = ParameterPacket(3, 255)
# waitInterval.SendPacket()
# time.sleep(2)
# print("updating wait interval to default (60)")
# waitInterval = ParameterPacket(3, 60)
# waitInterval.SendPacket()
# time.sleep(1)
# print("updating sonar delay to 0")
# waitInterval = ParameterPacket(4, 0)
# waitInterval.SendPacket()
# time.sleep(1)
# endregion TEST ARDUINO PARM UPDATE

# region Rotation Sensor Test

# magnetic declination: 5.88333 degrees, 0.1026834795 radians
# from http://www.magnetic-declination.com/
# declinationAngle = 0.103
#
# headingArray = []
# accelTimeArray = []
# timeArray = []
# countArray = []
# compassStuffX = []
# compassStuffY = []
# compassStuffZ = []
# gyroX = []
# gyroY = []
# gyroZ = []
# accelX = []
# accelY = []
# accelZ = []
# tempStuff = []
# pressureStuff = []
# sonarC = []
# sonarL = []
# sonarR = []
#
# prevSec = 0
# interval = 0.1
# count = 0
# total = 20
# print("\nGetting sensor data")
# startTime = time.time()
# while count < total:  # to the right
#     nowTime = time.time()
#     if nowTime - prevSec > interval:
#         count += 1
#         prevSec = nowTime
#         print("\n\nCount: ", count)
#         countArray.append(count)
#         timeArray.append(time.time() - startTime)
#         accelTimeArray.append(time.time() - startTime)
#         # read and log sensor data
#         # request sensor data packet
#         thisPacket = PacketFile.SensorPacket(7)
#         consume = Consume.ConsumePacket()
#
#         # COMPASS ///////////////////////////////////////
#         # print("============ Compass Data: ============")
#         compassStuffX.append(consume[1])
#         compassStuffY.append(consume[2])
#         compassStuffZ.append(consume[3])
#
#         # GYRO ///////////////////////////////////////
#         # print("============ Gyroscope Data: ============")
#         gyroX.append(consume[4])
#         gyroY.append(consume[5])
#         gyroZ.append(consume[6])
#
#         # TEMP AND PRESSURE ///////////////////////////////////////
#         # print("============ Temp and Pressure Data: ============")
#         tempStuff.append(consume[10])
#         pressureStuff.append(consume[11])
#
#         # SONAR ///////////////////////////////////////
#         # print("============ Sonar Data: ============")
#         sonarC.append(consume[12])
#         sonarR.append(consume[13])
#         sonarL.append(consume[14])
#
#         # ACCELEROMETER BEFORE ///////////////////////////////////////
#         # print("============ Accelerometer Data: ============")
#         accelX.append(consume[7])
#         accelY.append(consume[8])
#         accelZ.append(consume[9])
#
#         Motion.MoveRover(50, 2)
#         # time.sleep(0.1)
#         # # ACCELEROMETER DURING ///////////////////////////////////////
#         # print("============ Accelerometer Data: ============")
#         # thisPacket = SensorRequestPacket(7)
#         # thisPacket.SendPacket()
#         # consume = Consume.ConsumePacket()
#         # accelX.append(consume[7])
#         # accelY.append(consume[8])
#         # accelZ.append(consume[9])
#         # accelTimeArray.append(time.time() - startTime)
#         # time.sleep(0.1)
#         # Motion.StopRover()
#         #
#         # # ACCELEROMETER AFTER ///////////////////////////////////////
#         # print("============ Accelerometer Data: ============")
#         # thisPacket = SensorRequestPacket(7)
#         # thisPacket.SendPacket()
#         # consume = Consume.ConsumePacket()
#         # accelX.append(consume[7])
#         # accelY.append(consume[8])
#         # accelZ.append(consume[9])
#         # accelTimeArray.append(time.time() - startTime)
#
# print("\n\nTurning left now\n\n")
#
# while count < total * 2:  # to the left
#     nowTime = time.time()
#     if nowTime - prevSec > interval:
#         count += 1
#         prevSec = nowTime
#         print("\n\nCount: ", count)
#         countArray.append(count)
#         timeArray.append(time.time() - startTime)
#         accelTimeArray.append(time.time() - startTime)
#         # read and log sensor data
#         # request sensor data packet
#         thisPacket = PacketFile.SensorPacket(7)
#         consume = Consume.ConsumePacket()
#
#         # COMPASS ///////////////////////////////////////
#         # print("============ Compass Data: ============")
#         compassStuffX.append(consume[1])
#         compassStuffY.append(consume[2])
#         compassStuffZ.append(consume[3])
#
#         # GYRO ///////////////////////////////////////
#         # print("============ Gyroscope Data: ============")
#         gyroX.append(consume[4])
#         gyroY.append(consume[5])
#         gyroZ.append(consume[6])
#
#         # TEMP AND PRESSURE ///////////////////////////////////////
#         # print("============ Temp and Pressure Data: ============")
#         tempStuff.append(consume[10])
#         pressureStuff.append(consume[11])
#
#         # SONAR ///////////////////////////////////////
#         # print("============ Sonar Data: ============")
#         sonarC.append(consume[12])
#         sonarR.append(consume[13])
#         sonarL.append(consume[14])
#
#         # ACCELEROMETER BEFORE ///////////////////////////////////////
#         # print("============ Accelerometer Data: ============")
#         accelX.append(consume[7])
#         accelY.append(consume[8])
#         accelZ.append(consume[9])
#
#         Motion.MoveRover(50, 3)
#         # time.sleep(0.1)
#         # ACCELEROMETER DURING ///////////////////////////////////////
#         # print("============ Accelerometer Data: ============")
#         # thisPacket = SensorRequestPacket(7)
#         # thisPacket.SendPacket()
#         # consume = Consume.ConsumePacket()
#         # accelX.append(consume[7])
#         # accelY.append(consume[8])
#         # accelZ.append(consume[9])
#         # accelTimeArray.append(time.time() - startTime)
#         # time.sleep(0.1)
#         # Motion.StopRover()
#
#         # ACCELEROMETER AFTER ///////////////////////////////////////
#         # print("============ Accelerometer Data: ============")
#         # thisPacket = SensorRequestPacket(7)
#         # thisPacket.SendPacket()
#         # consume = Consume.ConsumePacket()
#         # accelX.append(consume[7])
#         # accelY.append(consume[8])
#         # accelZ.append(consume[9])
#         # accelTimeArray.append(time.time() - startTime)
#
# Motion.StopRover()
# # headings in degrees:
# # South = 180, North = 360/0, East = 90, West = 270
# GreyMatterMagnetoception = GreyMatter.Magnetoception()
#
# # calculate heading in degrees to get basic cardinal directions yo
# for count in range(0, len(compassStuffX)):
#     headingArray.append(
#         GreyMatterMagnetoception.getSense(
#             compassStuffY[count], compassStuffX[count]))
#
#
# print("\n\npretty compass table:")
# table = []
# headers = ["Heading", "X", "Y", "Z", "Count", "Timestamp"]
#
# for count in range(0, len(compassStuffX)):
#     table.append([headingArray[count], compassStuffX[count], compassStuffY[count], compassStuffZ[count],
#                   countArray[count], "%.4f" % timeArray[count]])
# print(tabulate.tabulate(table, headers))
#
# print("\n\npretty gyro table:")
# table = []
# headers = ["X", "Y", "Z", "Count", "Timestamp"]
#
# for count in range(0, len(gyroX)):
#     table.append([gyroX[count], gyroY[count], gyroZ[count], countArray[count], "%.4f" % timeArray[count]])
# print(tabulate.tabulate(table, headers))
#
# print("\n\npretty accel table:")
# table = []
# headers = ["X", "Y", "Z", "Count", "Timestamp"]
#
# counter = 0
# for count in range(0, len(accelX)):
#     counter += 1
#     table.append([accelX[count], accelY[count], accelZ[count], counter, "%.4f" % accelTimeArray[count]])
# print(tabulate.tabulate(table, headers))
#
# print("\n\npretty temp and pressure table:")
# table = []
# headers = ["Temp", "Pressure", "Count", "Timestamp"]
#
# for count in range(0, len(tempStuff)):
#     table.append([tempStuff[count], pressureStuff[count], countArray[count], "%.4f" % timeArray[count]])
# print(tabulate.tabulate(table, headers))
#
# print("\n\npretty sonar table:")
# table = []
# headers = ["Center", "Right", "Left", "Count", "Timestamp"]
#
# for count in range(0, len(sonarC)):
#     table.append([sonarC[count], sonarR[count], sonarL[count], countArray[count], "%.4f" % timeArray[count]])
# print(tabulate.tabulate(table, headers))
#
# print("\n\npretty super duper table:")
# table = []
# headers = ["Compass Heading", "Compass X", "Compass Y", "Compass Z",
#            "Gyro X", "Gyro Y", "Gyro Z",
#             "Accel X", "Accel Y", "Accel Z",
#            "Center", "Right", "Left", "Count", "Timestamp"]
# for count in range(0, len(sonarC)):
#     table.append([headingArray[count], compassStuffX[count], compassStuffY[count], compassStuffZ[count],
#                   gyroX[count], gyroY[count], gyroZ[count],
#                   accelX[count], accelY[count], accelZ[count],
#                   sonarC[count], sonarR[count], sonarL[count], countArray[count], "%.4f" % timeArray[count]])
# print(tabulate.tabulate(table, headers))
#
# print("\n\npretty super duper table:")
# table = []
# headers = ["Compass Heading",
#            "Center", "Right", "Left", "Count", "Timestamp"]
# for count in range(0, len(sonarC)):
#     table.append([headingArray[count],
#                   sonarC[count], sonarR[count], sonarL[count], countArray[count], "%.4f" % timeArray[count]])
# print(tabulate.tabulate(table, headers))
#
# print("\n\nfin")

# endregion Rotation Sensor Test

# print("\nCompass Data total: ", len(compassStuffX))
# if len(compassStuffX) is not 0:
#     avgX = statistics.mean(compassStuffX)
#     avgY = statistics.mean(compassStuffY)
#     avgZ = statistics.mean(compassStuffZ)
#     print("\nAverage"
#           "\nX: ", avgX,
#           "\nY: ", avgY,
#           "\nZ: ", avgZ)
#
#     varX = statistics.variance(compassStuffX)
#     varY = statistics.variance(compassStuffY)
#     varZ = statistics.variance(compassStuffZ)
#     print("\nVariance"
#           "\nX: ", varX,
#           "\nY: ", varY,
#           "\nZ: ", varZ)
# ##################################### end sensor testing


# print("\nSonar Data total: ", len(sonar))
# if len(sonar) is not 0:
#     avgX = statistics.mean(sonar.x)
#     avgY = statistics.mean(sonar.y)
#     avgZ = statistics.mean(sonar.z)
#     print("\nAverage"
#           "\nX: ", avgX,
#           "\nY: ", avgY,
#           "\nZ: ", avgZ)
#
#     varX = statistics.variance(sonar.x)
#     varY = statistics.variance(sonar.y)
#     varZ = statistics.variance(sonar.z)
#     print("\nVariance"
#           "\nX: ", varX,
#           "\nY: ", varY,
#           "\nZ: ", varZ)
#
#     stdX = statistics.stdev(sonar.x)
#     stdY = statistics.stdev(sonar.y)
#     stdZ = statistics.stdev(sonar.z)
#     print("\nStandard Deviation"
#           "\nX: ", stdX,
#           "\nY: ", stdY,
#           "\nZ: ", stdZ)

# print("Sonar Raw:")
# for item in sonar.x:
#     print("X: ", item)
# for item in sonar.y:
#     print("Y: ", item)
# for item in sonar.z:
#     print("Z: ", item)

# MoveRover(25, 2)
# time.sleep(2)
# MoveRover(25, 3)
# time.sleep(2)
# StopRover()

# packet = StopRover()
# packet.SendPacket()
# time.sleep(6)  # wait for arduino to boot after serial connection made
#
# packet = MotorPacket(58, 1, 0, 100)  # MoveRover(1, 100)
# packet.SendPacket()
# time.sleep(4)
#
# packet = MoveRover(0, 100)
# packet.SendPacket()
# time.sleep(4)

# packet = StopRover()
# packet.SendPacket()
