<!-- TOC -->

- [Communications Between Pi and Arduino](#communications-between-pi-and-arduino)
    - [Packet Breakdown](#packet-breakdown)
        - [Row 1 : byte[0]](#row-1--byte0)
        - [Row 2 : byte[1]](#row-2--byte1)
            - [Packet Destination](#packet-destination)
        - [Row 3 : byte [2]](#row-3--byte-2)
            - [Motor Commands](#motor-commands)
        - [~~Parms~~](#parms)
        - [Sensor Request Command](#sensor-request-command)
        - [All Sensor Data Packet](#all-sensor-data-packet)

<!-- /TOC -->

# Communications Between Pi and Arduino

## Packet Breakdown

### Row 1 : byte[0]

> Header

header = 58

### Row 2 : byte[1]

#### Packet Destination

- Parms = 0
- Motors = 1
- Sensor Request = 2

### Row 3 : byte [2]

#### Motor Commands

- Direction
  - Stop = 0
  - Forward = 1
  - Backward = 2
  - Unchanged = 3

- 

### ~~Parms~~

- ~~shut the fuck up = 1 (depreciated, i think. don't really need ACKs?)~~

### Sensor Request Command

- Sensor
  - compass = 0
  - gyro = 1
  - accel = 2
  - temp = 3
  - pressure = 4
  - all of the above (10DOF) = 5
  - sonar sensors = 6
  - 10DOF and sonar = 7 (default) (this is the only one that works, arduino doesn't respond to any other sensor request)

### All Sensor Data Packet

This is the packet structure when we receive from the arduino:

| name                   | byte array position   | value or type |
| ---------------------- | --------------------- | ------------- |
| header                 | 0                     | 58 (float)    |
| compass X              | 1                     | float         |
| compass Y              | 2                     | float         |
| compass Z              | 3                     | float         |
| gyro X                 | 4                     | float         |
| gyro Y                 | 5                     | float         |
| gyro Z                 | 6                     | float         |
| accel X                | 7                     | float         |
| accel Y                | 8                     | float         |
| accel Z                | 9                     | float         |
| temperature (C)        | 10                    | float         |
| pressure (hectopascal) | 11                    | float         |
| sonar center           | 12                    | float         |
| sonar right            | 13                    | float         |
| sonar left             | 14                    | float         |
| EOL                    | 15                    | 200.200       |

or

What the current (30 Jul 2016 05:08 PM) packet received from arduino looks like:
![office lens 20160721-094429](https://cloud.githubusercontent.com/assets/3520576/17273151/67dff0b0-5679-11e6-9606-ebe9a608e800.jpg)
