<!-- TOC -->

- [Packet Infrastructure](#packet-infrastructure)
    - [Header 🤕](#header-🤕)
        - [Version](#version)
            - [Major](#major)
            - [Minor](#minor)
        - [Destination](#destination)
            - [Parent](#parent)
            - [Child](#child)
        - [Source](#source)
        - [Packet Length](#packet-length)
        - [Payload Type](#payload-type)
        - [ACK/Sequence Number](#acksequence-number)
        - [Checksum](#checksum)
    - [Payload](#payload)
        - [Sensor Data Request](#sensor-data-request)

<!-- /TOC -->

# Packet Infrastructure

> Current Version: v1.0
> 2016-12-23T1526-0500

This file will map out the packet infrastructure of the communication between the arduino and the RPi.

The packet will be broken up into 2 parts: Header and Payload. The Header will contain information about the packet itself
and how to read the packet. The Payload will contain specific data based on what is intended to be communicated between the 
RPi and the arduino.

## Header 🤕

The lists bellow draw out each piece of a header and gives descriptions for each item.

| Packet Item                         | Description                                                                             | Example Value | Notes                   |
| ----------------------------------- | --------------------------------------------------------------------------------------- | ------------- | ----------------------- |
| version                             | Version of packet infrastructure. Major breaks backwards compatibility, minor will not. | 0xabcd        | ab = major, cd = minor  |
| destination                         | Destination address of packet                                                           | 0xabcd        | ab = parent, cd = child |
| source                              | Source address of packet                                                                | 0xabcd        | ab = parent, cd = child |
| payload types                       | IDs the type of payload                                                                 | 0xabcd        | ab = major, cd = minor  |
| ACK #                               | ACK # from a sequence of messages. This # will increment the last rx ACK #              | 0xffff        |                         |
| length of packet (header + payload) | Sum of bytes in header AND payload                                                      | 0xffff        | 65535 total             |
| cobs?                               |                                                                                         | ??            |                         |
| checksum                            | Remainder of (Sum of values (of each byte) / 32)                                        | 0xff          |                         |

| Octet (bit) offsets | 1 (8)           | 2 (16)          | 3 (24)               | 4 (32)              |
| ------------------- | --------------- | --------------- | -------------------- | ------------------- |
| 0 (0)               | Version (Major) | Version (Minor) | Destination (Parent) | Destination (Child) |
| 4 (32)              | Source (Parent) | Source (Child)  | Packet Length        | Packet Length       |
| 8 (64)              | Payload Type    | Payload Type    | ACK/Sequence #       | ACK/Sequence #      |
| 12 (96)             | Zeros           | Zeros           | Checksum             | Checksum            |

| Packet Item | Sub Item |    |    |
| ----------- | -------- | -- | -- |
| Version     | Major         |    |    |
|             | Minor |    |    |

### Version

This value indicates the version of the communication protocol. This will most likely be changed only a handful of times.

#### Major

This value breaks backwards compatibility and MUST match on received packet.

#### Minor

This value DOES NOT break backwards compatibility and DOES NOT need to match the value on the received packet.

### Destination

#### Parent

This value is to identify the device destination. IE Arduino or RPi

#### Child

This value identifies the destination within the device.


Parent: Device ID (RPi, macid? serial?)
Child: sub device (motors, leds, sensor)

### Source

Parent: Device ID
Child:

### Packet Length

### Payload Type

### ACK/Sequence Number

### Checksum

## Payload

Multiple types of payloads exist in this infrastructure. They are:

- Sensor
- Motor
- Communication
- LED
- Undefined Payload Type 1
- Undefined Payload Type 2

OR???

I think im gonna go with this one

- Get
- Set

### Sensor Data Request

| Octet (bit) offsets | 1 (8)           | 2 (16)          | 3 (24)                 | 4 (32)              |
| ------------------- | --------------- | --------------- | ---------------------- | ------------------- |
| 0 (0)               | Version (Major) | Version (Minor) | Destination   (Parent) | Destination (Child) |
| 4 (32)              | Source (Parent) | Source (Child)  | Length                 | Length              |
| 8 (64)              | Payload Type    | Payload Type    | Checksum               | Checksum            |

note: the offsets will be used to appropriately 'mask' the bits in the packets we create and consume.